from pylab import *
from lmfit import Model
from numpy import *

#-------------------------------------------
def decay(indep_v, N, tau):
	t = indep_v
	return N*exp(-t/tau)
#-------------------------------------------

t 	= linspace(0, 5, num=1000)
data 	= decay(t, 7, 3) + random.randn(*t.shape)

#-------------------------------------------
model 	= Model(decay, independent_vars=['indep_v'])
params	= model.params()

params['N'].value 	= 10  		# initial guess
params['tau'].value 	= 2.9		# seed / initial value
params['tau'].min 	= 0		# bound min
params['tau'].max 	= pi		# bound max
params['tau'].vary	= False		# "FIXED" param

result 	= model.fit(data, params, indep_v=t)
""" 
Otras maneras de asginar valores a los elements de la clase 'model':
result 	= model.fit(data, t=t, N=10, tau=1)
result = model.fit(data, t=t, 
			N=Parameter(value=10), 
			tau=Parameter(value=1))
"""
#-------------------------------------------
print result.values
print "\n"

plot(t, data)  # data
plot(t, decay(indep_v=t, **result.values), lw=5)  # best-fit model

print result.params

show()
