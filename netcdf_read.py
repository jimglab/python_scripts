from scipy.io import netcdf
f = netcdf.netcdf_file('simple.nc', 'r')
print(f.history)

time = f.variables['time']

print(time.units)

print(time.shape)

print(time[-1])

f.close()
