#!/usr/bin/env python
# -*- coding: utf-8 -*-
import pandas as pd
import numpy as np
import multiprocessing as mp
import copy
"""
Sources:
* https://stackoverflow.com/questions/5442910/python-multiprocessing-pool-map-for-multiple-arguments
"""


def my_func(x):
  print(mp.current_process())
  return x**x


def myfunc2(df, lim):

    dff = copy.deepcopy(df)
    dff["prod"] = np.power(
        dff["uno"] * dff["rand"]**2 - np.e,
        2.666 * np.pi)

    return dff.iloc[lim[0]:lim[1]]


n = 9000000
df_src = pd.DataFrame({
    "uno"  : np.arange(22, 22+n),
    "rand" : np.random.rand(n),
    })


pool = mp.Pool(mp.cpu_count())
result = pool.starmap(myfunc2, [
    [df_src, (11, 31)], 
    [df_src, (33, 44)],
    ])

#result = pool.map(my_func, [4,2,3,5,3,2,1,2])
#result_set_2 = pool.map(my_func, [4,6,5,4,6,3,23,4,6])

print(result)

#EOF
