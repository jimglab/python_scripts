""" 
Let's say I want to use SimpleQueue, with a producer process that eventually (*) inserts tasks into the queue, and a consumer process that is constantly checking for new tasks in the queue.

Gimme an implementation of that.

(*): during some periods of time, the producer puts some tasks, and in other time periods the producer doesn't. So the queue can be empty sometimes but the consumer shouldn't terminate.

Source:
    https://chat.openai.com/c/6119088b-0913-492f-8559-458b333a7b1c
"""
from multiprocessing import Process, SimpleQueue
import time
import random

def producer(q):
    for i in range(10):  # Let's say the producer produces 10 tasks.
        time.sleep(random.random())  # Simulate time periods with no production.
        q.put(f'Task {i}')
        print(f'Produced: Task {i}')
    q.put('DONE')  # Signal that no more tasks will be produced.

def consumer(q):
    while True:
        task = q.get()
        if task == 'DONE':
            print('No more tasks to consume.')
            break
        print(f'Consumed: {task}\n')
        time.sleep(random.random())  # Simulate task processing time.

def main():
    q = SimpleQueue()

    producer_process = Process(target=producer, args=(q,))
    consumer_process = Process(target=consumer, args=(q,))

    producer_process.start()
    consumer_process.start()

    producer_process.join()
    consumer_process.join()

if __name__ == '__main__':
    main()

#EOF
