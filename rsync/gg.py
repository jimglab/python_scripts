#!/usr/bin/python
import os

inputs	= []
inputs	+= [[ ' host ([localhost]/.)	:', 'localhost']]
inputs  += [[ ' port [2222]		:', '2222']]
inputs  += [[ ' user [masiasmj]	:', 'masiasmj']]
inputs  += [[ ' backup? ([y]/n)	:', 'y']]
inputs  += [[ ' dir_src		:', '']]
inputs  += [[ ' dir_dst		:', '']]
ninputs	= len(inputs)

def ask_input(i, inp):
	try:
		inpp	= raw_input(inputs[i][0])	# cualquier cosa q escriba la
							# interpreta como string :-)
		if inpp=='':
			if ('dir_' in inputs[i][0]):
				ask_input(i, inp)
			else:
				inp	+= [ inputs[i][1] ]
		else:
			inp	+= [ inpp ]

	except KeyboardInterrupt:
		print "\n KEYBOARD......."
		raise SystemExit

#---------------------------------------------------------------------------------
inp	= []
for i in range(ninputs):
	ask_input(i, inp)

#---------------------------------------------------------------------------------
hostname        = inp[0]
ssh_port	= int(inp[1])		# nro entero
username        = inp[2]

if hostname!='.':
	ssh	= '-e "ssh -p %d"' % ssh_port
	ssh	= ssh + ' ' + username + '@' + hostname + ':'
else:
	ssh 	= ''
#---------------------------------------------------------------------------------
bckp		= inp[3]
if bckp=='y':
	bckp_suffix     = u'-b --suffix=_bckp_`hostname`_`date +%d%b%Y_%H.%M.%S`'
else:
	bckp_suffix     = ''
#---------------------------------------------------------------------------------
dir_src		= inp[4]
dir_dst		= inp[5]
#---------------------------------------------------------------------------------

options		= '-rvuth --human-readable --progress'
RSYNC_COMMAND 	= 'rsync %s %s %s%s "%s"' % (options, bckp_suffix, ssh, dir_src, dir_dst)

print " EJECUTANDO:\n", RSYNC_COMMAND
os.system(RSYNC_COMMAND)


