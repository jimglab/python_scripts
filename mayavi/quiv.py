import numpy as np
from numpy.random import random
from numpy import ones
from mayavi import mlab
#x, y, z, s, c = np.random.random((5, 10))
x, y, z = random((3, 10))
c = random(x.size)
#s = 50.*random(x.size)
s = 50.*ones(x.size)
pts = mlab.quiver3d(x, y, z, s, s, s, scalars=c, mode='sphere', scale_factor=0.001)
pts.glyph.color_mode = 'color_by_scalar'
# Finally, center the glyphs on the data point
pts.glyph.glyph_source.glyph_source.center = [0, 0, 0]
