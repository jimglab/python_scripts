from mayavi import mlab
from numpy import random

import matplotlib
matplotlib.use('WxAgg')
matplotlib.interactive(True)

@mlab.show
def image():
   mlab.imshow(random.random((10, 10)))

image()
