#!/usr/bin/env python3
import matplotlib.pyplot as plt
from PIL import Image
import numpy as np
import pandas as pd

plt.style.use('dark_background')

img = Image.open('./good.bad.john.png')

n_panels = 2
ncols = 2
fig = plt.figure(figsize=(11, 4*n_panels))

ax = []
for i in range(n_panels*ncols):
    ax += [fig.add_subplot(n_panels, ncols, i+1)]

ax[0].imshow(img)

x = np.linspace(0, np.pi, num=100)
y = x*x
ax[1].plot(x, y)
ax[2].plot(x, y, '--r')
ax[3].plot(x, y, '.-g')

#EOF
