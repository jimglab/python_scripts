from numpy import shape
from Scientific.IO.NetCDF import NetCDFFile
import numpy as np
 
# Write a variable with rectilinear coordinates to NETCDF.
def write_rectilinear(file, var, varname, x_coord, y_coord):
    nx = len(x_coord)
    ny = len(y_coord)
 
    # Reshape for 2D
    my_data = np.reshape(var,(ny,nx),'C') 	# si le pones 'F', no guarda bien a 'data' :S
 
    # Create dimensions:
    file.createDimension('nx_dim', nx)
    file.createDimension('ny_dim', ny)
 
    x = file.createVariable('nx', 'd', ('nx_dim',))	# 'd' for "double"
    y = file.createVariable('ny', 'd', ('ny_dim',)) 	# ""
    y.description 	= 'fref'
    y.untis 		= 'unidades de y :P'
 
    # transfer the coordinate variables:
    x[:] = x_coord
    y[:] = y_coord
 
    # Create data variable in NetCDF.
    dummy = file.createVariable(varname, 'd', ('ny_dim','nx_dim'))
 
    # transfer the data variables:
    dummy[:] 		= my_data
    dummy.description 	= 'esto es una matriz de doubles'
    dummy.units		= 'unidades del array'
 
 
# coordinate information:
x_coord = [0,2,4,6,8,10,12] 
y_coord = [0, 5, 10]
 
# number of points:
nx = len(x_coord)
ny = len(y_coord)
 
# Create a nodal data variable
nodal = []
for i in range(nx):
    for j in range(ny):
        nodal += [j*nx + i - .33]
 
# Write the file.
fname_out	= "rectilinear.nc"
file = NetCDFFile(fname_out, 'w')
write_rectilinear(file, nodal, 'mydata', x_coord, y_coord)
print " hemos creado %s" % fname_out
file.close()
