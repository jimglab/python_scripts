import numpy as np
from mpi4py import MPI

comm 	= MPI.COMM_WORLD
rank 	= comm.Get_rank()
wsize	= comm.Get_size()

vv	= np.array(np.linspace(rank, rank+3, 4))
P0	= ' rank: %d/%d ---> '% (rank, wsize)
P1	= '%f' % vv[3]
print P0 + P1 +  '\n'
# mpirun -np 4 python test_mpi.py
