#!/usr/bin/env python

# Esto es para comprobar q ambos procesadores
# NO comparten la misma memoria de la clase Mapping().
# Si yo corriera esto en un solo procesador, el ...append() 
# SI deberia afectar a 'a' y 'b'.
#
import ww
from mpi4py import MPI                                                                                                         

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
print ' jelooouu from %d\n'% rank


if rank==0:

    a = ww.Mapping(range(4))
    a.other.append(111111)
    print " [r:%d] " % rank
    print " a: ", a.other

if rank==1:
    b = ww.Mapping(range(9))

    #aa = raw_input(' GIMME STHING: ')
    #print "hehehe", aa, '\n'
    print " [r:%d] " % rank
    print " b: ", b.other


# mpirun -np 4 python test_mpi.py

