#!/usr/bin/env ipython
# -*- coding: utf-8 -*-
from pylab import pause
from mpi4py import MPI
from os.path import isfile, isdir
import os
from h5py import File as h5
from numpy.random import rand, random
import numpy as np

comm  = MPI.COMM_WORLD
rank  = comm.Get_rank()
wsize = comm.Get_size()

fname_out = 'test.h5'
if rank==0 and isfile(fname_out):
    os.system('mv {fnm} {fnm}_'.format(fnm=fname_out))

comm.Barrier()

#for r in range(wsize):
    #if rank==r:
if isfile(fname_out):
    pause(0.2*(rank + 1))
    fo = h5(fname_out, 'r+') # append
else:
    fo = h5(fname_out, 'w')  # create

nx, ny = int(rand()*1e3), int(rand()*1e3)
m = random((nx, ny))
print " [r:%d] may i?" % rank

while 'mine' in fo.keys():
    pause(0.1)
    fo = h5(fname_out, 'r+') # read again

print " [r:%d] now i'll write" % rank

#pause(0.2)
fo['mine'] = 1    # file ocupado
fo['numbers/%d_%d'%(nx, ny)] = m
pause(0.1)
fo.pop('mine')    # libera file

fo.close()

if rank==0:
    os.system('rm %s_' % fname_out)
