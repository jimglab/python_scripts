#!/usr/bin/env ipython
# -*- coding: utf-8 -*-
"""
ESTO FUNCIONA!!!!!
"""
from pylab import pause
from mpi4py import MPI
from os.path import isfile, isdir
import os
from h5py import File as h5
from numpy.random import rand, random
import numpy as np

def w2f(rank, fname, var, vname, mode='r+'):
    try:
        pause(0.2*(rank+1))
        f = h5(fname, mode=mode)
        f[vname] = var
        f.close()
        print " [r:%d] ########## TERMINE ########## " % rank
    except NameError:
        pause((rank+1)*0.3)
        f.close()
        w2f(rank, fname, var, vname, mode='r+')
    except RuntimeError as e:
        if e.message.startswith("Unable to create link"):
            pass
        pause((rank+1)*0.3)
        if isfile(fname):
            w2f(rank, fname, var, vname, mode='r+')
        else:
            w2f(rank, fname, var, vname, mode='w')
    except IOError as e:
        print " [r:%d] MSG: %s" % (rank, e.message)
        #print " NRO: ", e.errno
        #print " ARG: ", e.args
        pause((rank+1)*0.3)
        print " [r:%d] --->"%rank, dir(e)
        #if e.message.startswith("Unable to open file"):
        if isfile(fname):
            w2f(rank, fname, var, vname, mode='r+')
        else:
            w2f(rank, fname, var, vname, mode='w')
        #if e.message.startswith("Unable to create file"):
        #    #f.close()
        #    w2f(rank, fname, var, vname, mode='r+')
        #elif e.message.startswith("Unable to open file"):
        #    w2f(rank, fname, var, vname, mode='w')


def w2file(rank, fname, var, vname, mode='r+'):
    try:
        pause(0.2*(rank+1))
        f = h5(fname, mode=mode)
        f[vname] = var
        f.close()
        print " [r:%d] ########## TERMINE ########## " % rank
    except NameError:
        pause((rank+1)*0.3)
        f.close()
        w2f(rank, fname, var, vname, mode='r+')
    except RuntimeError as e:
        if e.message.startswith("Unable to create link"):
            pass
        pause((rank+1)*0.3)
        if isfile(fname):
            w2f(rank, fname, var, vname, mode='r+')
        else:
            w2f(rank, fname, var, vname, mode='w')
    except IOError as e:
        print " [r:%d] MSG: %s" % (rank, e.message)
        pause((rank+1)*0.3)
        #print " [r:%d] --->"%rank, dir(e)
        if isfile(fname):
            w2f(rank, fname, var, vname, mode='r+')
        else:
            w2f(rank, fname, var, vname, mode='w')

comm  = MPI.COMM_WORLD
rank  = comm.Get_rank()
wsize = comm.Get_size()

a = rank*np.ones(20)
w2file(rank, 'test.h5', a, 'a%02d'%rank)


