#!/usr/bin/env ipython
# -*- coding: utf-8 -*-
from pylab import pause
from mpi4py import MPI

comm  = MPI.COMM_WORLD
rank  = comm.Get_rank()
wsize = comm.Get_size()

for r in range(wsize):
    if rank==r:
        print " --> I'm rank: %d" % rank

if rank==0:
    print ' ---> I will wait :-) '
    pause(2) 

comm.Barrier()
for r in range(wsize):
    if rank==r:
        print " --> I'm going: %d" % rank


for r in range(wsize):
    if rank!=r:
        comm.Barrier()
        #print " [r:%d] out of barrier" % rank
        continue

    pause(2)
    print " r:%d " % rank
    #comm.Barrier()
