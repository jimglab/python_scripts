#!/usr/bin/env ipython
# -*- coding: utf-8 -*-
from pylab import *
from datetime import datetime, timedelta
from mpi4py import MPI
import numpy as np

date1	= datetime(2006  , 1, 1)
date2	= datetime(2013+1, 1, 1)


#------------------------------------------------------------MPI
comm	= MPI.COMM_WORLD
rank	= comm.Get_rank()
wsize	= comm.Get_size()
#---------------------------------------------------------------

tot	= ((date2 - date1).total_seconds())/86400.
dtot	= int(tot/wsize)

if rank==0:
	print "\n ---> tot: %f"  % tot

if rank!=wsize-1:
	ini	= rank*dtot
	fin	= (rank+1)*dtot
	print "\n [r:%d] my jobs IDs, ini/fin: %d/%d" % (rank, ini, fin)

if rank==wsize-1:
	ini	= rank*dtot
	fin 	= tot
	print "\n [r:%d] my jobs IDs, ini/fin: %d/%d" % (rank, ini, fin)

