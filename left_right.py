from pylab import *
from numpy import *

N = 100

# Create some random data for plotting on left axis and for the right axis
yLeft  = random.randn(N) + linspace(-25,25,N)
yRight = linspace(-50,50,N)**2

# Create left axis plots
axL = subplot(1,1,1)
plot(yLeft, '.-b')
grid(True)
ylabel("Left Y-Axis Data")
xlabel("X-Axis Data")
title("Data Plotted on Left and Right Axis")

# Create right axis and plots.  It is the frameon=False that makes this
# plot transparent so that you can see the left axis plot that will be
# underneath it.  The sharex option causes them to share the x axis.
axR = subplot(1,1,1, sharex=axL, frameon=False)
axR.yaxis.tick_right()
axR.yaxis.set_label_position("right")
plot(yRight, '.-g')
ylabel("Right Y-Axis Data")

# save the figure
savefig("left_right.png", dpi=72)
show()
