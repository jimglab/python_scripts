import numpy as np
import matplotlib.pyplot as plt

fig = plt.figure()
ax1 = fig.add_subplot(111)

x = np.linspace(1, 600, 60.)
a = np.cos(2*np.pi*np.linspace(0, 1, 60.))
ax1.plot(x, a)

ax1.set_xlim(0, 600)
ax1.set_xlabel("x")
ax1.set_ylabel("y")
#ax1.set_xscale('log')

ax2 = ax1.twiny()
ax2.set_xlabel("x-transformed")
ax2.set_xlim(0, 60)
ax2.set_xticks([10, 400, 500])
ax2.set_xticklabels(['7','8','99'])
#ax2.set_xscale('log')

plt.show()
