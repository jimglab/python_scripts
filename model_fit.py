import numpy
from scipy.optimize import curve_fit

class FitModel(object):
	def f(self, x, a, b, c):
		return x * 2*a + 4*b - 5*c

	def f_a(self, x, b, c):
		return self.f(x, self.a, b, c)

# user supplies a = 1.0
fitModel = FitModel()
fitModel.a = 1.0

xdata = numpy.array([1,3,6,8,10])
ydata = numpy.array([  0.91589774,   4.91589774,  10.91589774,  14.91589774,  18.91589774])
initial = (1.0,2.0)
popt, pconv = curve_fit(fitModel.f_a, xdata, ydata, initial)
