#!/usr/bin/env ipython
# -*- coding: utf-8 -*-
from pylab import *
import matplotlib.pyplot as plt 
import numpy as np
from matplotlib import cm
#from matplotlib.ticker import LinearLocator, FormatStrFormatter
from matplotlib.colors import LogNorm


def make_plot(stf, fname_fig=None):
    xlim         = stf['xlims'] # tuple
    ylim         = stf['ylims'] # tuple
    CBMIN, CBMAX = stf['cblims']
    title        = stf['title']
    LABEL        = stf['label']
    xlabel       = stf['xlabel']
    ylabel       = stf['ylabel']
    CB_LABEL     = stf['cb_label']
    levels       = stf['z_levels']
    x, y, z      = stf['xyz']

    fig     = figure(1, figsize=(6, 4))
    ax      = fig.add_subplot(111)

    surf    = ax.contourf(x, y, z.T, facecolors=cm.jet(z), linewidth=0, 
              levels=levels,
              cmap=cm.jet, alpha=0.9, vmin=CBMIN, vmax=CBMAX,)
              # norm=LogNorm())
    m = cm.ScalarMappable(cmap=surf.cmap, norm=surf.norm)
    m.set_array(z)

    axcb = plt.colorbar(m)
    axcb.set_label(CB_LABEL, fontsize=14)
    ax.set_xlabel(xlabel, fontsize=17)
    ax.set_ylabel(ylabel, fontsize=19)
    ax.set_title(title, fontsize=18)
    ax.set_ylim(ylim[0], ylim[1])   # no deberia necesitarlo
    #ax.set_xlim(xlim[0], xlim[1])  # no deberia necesitarlo
    ax.legend(loc='best', fontsize=15)

    m.set_clim(vmin=CBMIN, vmax=CBMAX)
    ax.grid()
    plt.tight_layout()

    if fname_fig is not(None):
        savefig(fname_fig, format='png', dpi=135, bbox_inches='tight')
        print(" --> generado: %s" % fname_fig)

    #close()
    return fig, ax

#EOF
