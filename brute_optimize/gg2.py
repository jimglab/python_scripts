#!/usr/bin/env ipython
# -*- coding: utf-8 -*-
""" 
example to minimize a function by "brute
force" method :)
"""
from pylab import show, close, savefig
import numpy as np
import funcs as ff
from numpy import (
    exp, cos, sin, tanh, pi, linspace
)
from scipy.optimize import brute, fmin

def fun(xy, a):
    x, y = xy
    o = (sin(x)**2)*cos(y)**2
    o *= (x+1.)*(x+2.)
    #o *= 1.0 + 0.8*tanh(y/pi)
    o *= (y-pi)*(y+pi)
    o *= 1.0*a
    return o

bins = 100, 100
x = np.linspace(0.0, pi, bins[0])
y = np.linspace(-2.*pi, 2*pi, bins[1])

z = np.empty((x.size, y.size))
for i in range(x.size):
    xy = x[i], y
    z[i,:] = fun(xy, 1.)

stf = {}
stf['xlims']    = x[0], x[-1]
stf['ylims']    = y[0], y[-1]
stf['cblims']   = z.min(), z.max()
stf['title']    = 'my_title'
stf['label']    = 'this label'
stf['xlabel']   = 'xlabel'
stf['ylabel']   = 'ylabel'
stf['cb_label'] = 'cb-label'
stf['z_levels'] = np.linspace(z.min(), z.max(), 40)
stf['xyz']      = x, y, z

# dibujar funcion
#fig, ax = ff.make_plot(stf, './test.png')
fig, ax = ff.make_plot(stf, fname_fig=None)

# minimize function
rranges = ( 
    slice(x[0], x[-1], (x[-1]-x[0])/20),
    slice(y[0], y[-1], (y[-1]-y[0])/20),
)

rb = brute(fun, rranges, args=(1.,), full_output=True, finish=fmin)

xo, yo = rb[0]
ax.scatter(xo, yo, marker='*', s=3)

fig.savefig('./test.png', dpi=300)
close(fig)
#EOF
