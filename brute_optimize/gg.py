#!/usr/bin/env ipython
# -*- coding: utf-8 -*-
from pylab import *
import numpy as np
import funcs as ff
from numpy import exp, cos, sin, tanh, pi

def fun(x, y):
    #x, y = par
    o = (sin(x)**2)*cos(y)**2
    o *= (x+1.)*(x+2.)
    #o *= 1.0 + 0.8*tanh(y/pi)
    o *= (y-pi)*(y+pi)
    return o

bins = 100, 100
x = np.linspace(0.0, pi, bins[0])
y = np.linspace(-2.*pi, 2*pi, bins[1])

z = np.empty((x.size, y.size))
for i in range(x.size):
    z[i,:] = fun(x[i], y)

stf = {}
stf['xlims']    = x[0], x[-1]
stf['ylims']    = y[0], y[-1]
stf['cblims']   = z.min(), z.max()
stf['title']    = 'my_title'
stf['label']    = 'this label'
stf['xlabel']   = 'xlabel'
stf['ylabel']   = 'ylabel'
stf['cb_label'] = 'cb-label'
stf['z_levels'] = np.linspace(z.min(), z.max(), 40)
stf['xyz']      = x, y, z

fig, ax = ff.make_plot(stf, './test.png')
close(fig)

