#!/usr/bin/env ipython
# -*- coding: utf-8 -*-
""" 
example to minimize a function by "brute
force" method :)
--> 3D version
"""
from pylab import show, close, savefig
import numpy as np
import funcs as ff
from numpy import (
    exp, cos, sin, tanh, pi, linspace
)
from scipy.optimize import brute, fmin

def fun(xyz, a):
    x, y, z = xyz
    o = (sin(x)**2)*cos(y)**2
    o *= (x+1.)*(x+2.)
    #o *= 1.0 + 0.8*tanh(y/pi)
    o *= (y-pi)*(y+pi)
    o += z
    o *= 1.0*a
    return o

# minimize function
rranges = ( 
    slice(0., pi, pi/20),
    slice(-2.*pi, +2.*pi, 4.*pi/20),
    slice(1., 2.*pi, 2.*pi/20),
)

rb = brute(fun, rranges, args=(1.,), full_output=True, finish=None)

xo, yo, zo = rb[0]

#EOF
