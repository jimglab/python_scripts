#!/usr/bin/env ipython
# -*- coding: utf-8 -*-
from pylab import savefig, close, show, figure
import numpy as np
from numpy.random import random as rand

n   = 30
x   = rand(n)
y   = rand(n)

fig     = figure(1, figsize=(6, 4))
fig.subplots_adjust(bottom=0.1, hspace=0.2, right= 0.9,top=1.2)

# 1st plot
ax      = fig.add_subplot(3, 1, 1)
ax.plot(x, y, '-ok')

# 2nd plot
ax      = fig.add_subplot(3, 1, 2)
ax.scatter(x, y, marker='o', c='red', label='jimmy')
ax.legend(loc='center', bbox_to_anchor=(1.2, 0.5))

# 3rd plot
ax      = fig.add_subplot(3, 1, 3)
ax.plot(x, y, '-sr')

savefig('./test.png', bbox_inches='tight')
close()
