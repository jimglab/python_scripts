import numpy as np 
import matplotlib.pyplot as plt
from datetime import datetime
from datetime import timedelta
from pylab import savefig, show, close, figure


ets_10=np.load('/home/aldana.arruti/Tesis/validation/ets/gaussian_corr_dt5/etapadesarrollo_010.npy')
ets_20=np.load('/home/aldana.arruti/Tesis/validation/ets/gaussian_corr_dt5/etapadesarrollo_020.npy')
ets_30=np.load('/home/aldana.arruti/Tesis/validation/ets/gaussian_corr_dt5/etapadesarrollo_030.npy')
ets_40=np.load('/home/aldana.arruti/Tesis/validation/ets/gaussian_corr_dt5/etapadesarrollo_040.npy')
ets_50=np.load('/home/aldana.arruti/Tesis/validation/ets/gaussian_corr_dt5/etapadesarrollo_050.npy')
ets_60=np.load('/home/aldana.arruti/Tesis/validation/ets/gaussian_corr_dt5/etapadesarrollo_060.npy')
ets_70=np.load('/home/aldana.arruti/Tesis/validation/ets/gaussian_corr_dt5/etapadesarrollo_070.npy')
ets_80=np.load('/home/aldana.arruti/Tesis/validation/ets/gaussian_corr_dt5/etapadesarrollo_080.npy')
ets_90=np.load('/home/aldana.arruti/Tesis/validation/ets/gaussian_corr_dt5/etapadesarrollo_090.npy')
ets_100=np.load('/home/aldana.arruti/Tesis/validation/ets/gaussian_corr_dt5/etapadesarrollo_100.npy')

t_prono=['+5','+10','+15','+20','+25','+30','+35','+40','+45','+50','+55','+60','']
x=np.linspace(1,12,12)


fig=plt.figure(1,figsize=(14,24))
plt.subplots_adjust(bottom=0.1, hspace=0.2, right= 0.9,top=0.9)

plt.subplot(3,1,1)
plt.plot(x,ets_10[0,:], 'g-o',label='Box_Size=21x21')
plt.plot(x,ets_20[0,:], 'r-v',label='Box_Size=41x41')
plt.plot(x,ets_30[0,:], 'c-s',label='Box_Size=61x61')
plt.plot(x,ets_40[0,:], 'm-D',label='Box_Size=81x81')
plt.plot(x,ets_50[0,:], 'y-^',label='Box_Size=101x101')
plt.plot(x,ets_60[0,:], 'k-D',label='Box_Size=121x121')
plt.plot(x,ets_70[0,:], 'g-s',label='Box_Size=141x141')
plt.plot(x,ets_80[0,:], 'r-o',label='Box_Size=161x161')
plt.plot(x,ets_90[0,:], 'c-x',label='Box_Size=181x181')
plt.plot(x,ets_100[0,:], 'y-s',label='Box_Size=201x201')
plt.xlim(xmin=0,xmax=13)
plt.ylim(ymin=0,ymax=1)
plt.xticks(x,t_prono)
plt.xlabel('Tiempo de Pronostico (minutos)')
plt.grid(True)
#ax.legend(loc='best')
plt.title('ETS \n Etapa de Desarrollo Umbral Reflectividad= 15\n TREC: CC, Control calidad: Deteccion de Extremos')

plt.subplot(3,1,2)
plt.plot(x,ets_10[1,:], 'g-o',label='Box_Size=21x21')
plt.plot(x,ets_20[1,:], 'r-v',label='Box_Size=41x41')
plt.plot(x,ets_30[1,:], 'c-s',label='Box_Size=61x61')
plt.plot(x,ets_40[1,:], 'm-D',label='Box_Size=81x81')
plt.plot(x,ets_50[1,:], 'y-^',label='Box_Size=101x101')
plt.plot(x,ets_60[1,:], 'k-D',label='Box_Size=121x121')
plt.plot(x,ets_70[1,:], 'g-s',label='Box_Size=141x141')
plt.plot(x,ets_80[1,:], 'r-o',label='Box_Size=161x161')
plt.plot(x,ets_90[1,:], 'c-x',label='Box_Size=181x181')
plt.plot(x,ets_100[1,:], 'y-s',label='Box_Size=201x201')
plt.xlim(xmin=0,xmax=13)
plt.ylim(ymin=0,ymax=1)
plt.xticks(x,t_prono)
plt.xlabel('Tiempo de Pronostico (minutos)')
plt.grid(True)
#ax.legend(loc='best')
#lgd=ax.legend(loc='center right', bbox_to_anchor=(1,0.5))

plt.title('ETS \n Etapa de Desarrollo Umbral Reflectividad= 30\n TREC: CC, Control calidad: Deteccion de Extremos')


plt.subplot(3,1,3)
plt.plot(x,ets_10[2,:], 'g-o',label='Box_Size=21x21')
plt.plot(x,ets_20[2,:], 'r-v',label='Box_Size=41x41')
plt.plot(x,ets_30[2,:], 'c-s',label='Box_Size=61x61')
plt.plot(x,ets_40[2,:], 'm-D',label='Box_Size=81x81')
plt.plot(x,ets_50[2,:], 'y-^',label='Box_Size=101x101')
plt.plot(x,ets_60[2,:], 'k-D',label='Box_Size=121x121')
plt.plot(x,ets_70[2,:], 'g-s',label='Box_Size=141x141')
plt.plot(x,ets_80[2,:], 'r-o',label='Box_Size=161x161')
plt.plot(x,ets_90[2,:], 'c-x',label='Box_Size=181x181')
plt.plot(x,ets_100[2,:], 'y-s',label='Box_Size=201x201')
plt.xlim(xmin=0,xmax=13)
plt.ylim(ymin=0,ymax=1)
plt.xticks(x,t_prono)
plt.xlabel('Tiempo de Pronostico (minutos)')
plt.grid(True)
#ax.legend(loc='best')
plt.title('ETS \n Etapa de Desarrollo  Umbral Reflectividad= 50\n TREC: CC, Control calidad: Deteccion de Extremos')

#cbaxes = fig.add_axes([0.85, 0.2, 0.03, 0.6])
#lgd = fig.legend(loc = cbaxes)



plt.savefig('/home/aldana.arruti/Tesis/validation/ets/gaussian_corr_dt5/ETS_etapadesarrollo_boxes.png',dpi=250,format='png', bbox_inches='tight')
plt.close(fig)



