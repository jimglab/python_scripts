#!/usr/bin/env python
# -*- coding: utf-8 -*-
import numpy as np
from pylab import (
        savefig, close, show, figure,
        plot, scatter
        )
from test_wavelets import WaveletAnalysis

test_data = np.loadtxt('./nino3data.asc', skiprows=3)

nino_time = test_data[:, 0]
nino_dt = np.diff(nino_time).mean()
anomaly_sst = test_data[:, 2]

wa = WaveletAnalysis(anomaly_sst, time=nino_time, dt=nino_dt)

#++++++++++++++++++++++++++

fname_fig = './test.png'
t       = nino_time
rdata   = wa.reconstruction() # all scaler for default

sc2     = wa.scales[-50:]
rdata2  = wa.reconstruction(scales=sc2)

fig     = figure(1, figsize=(20, 4))
ax      = fig.add_subplot(111)

ax.plot(t, wa.data, 'k-o', ms=2)
ax.plot(t, rdata2, 'r', lw=3)
ax.grid()

savefig(fname_fig, bbox_inches='tight')
close()
