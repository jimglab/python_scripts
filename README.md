# PYTEST
Collection of working-examples for different python packages/libraries.
Some nice subdirectories to note below.

--- 
### argpase
script to pass command-line arguments to py script. Example:
```bash
$python gg.py --IDs 44,55,66 -i 333
```
where we can pass a list of numbers, and the script ```gg.py``` converts them into a list of int/floats (whatever we want).

---
### mpi
use MPI routines: Send, Recv, Bcast, Reduce, Barriers, Allgather, etc.

---
### multiple_pdf
create one figure layout (may include several panels) per PDF page.

---
=======
### signal
signal handling. For ex: SIGTERM (```kill -15```: default for ```kill```), SIGINT (keyboard interrupt ```Ctrl+C```), etc. 
Remember that SIGKILL (```kill -9```) can NOT be handled (?).

---
### memory_profilers
routines for profiling of memory use.

---
### classes
messing with classes: inheritance & others.

---
=======
### hdf5
handle .h5/HDF5 files.

---
