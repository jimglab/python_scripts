#!/usr/bin/env ipython
# -*- coding: utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt
from pylab import figure, close

from scipy.spatial.distance import euclidean
import argparse

import dtw as dtw_funcs

plt.style.use("dark_background")


#--- retrieve args
parser = argparse.ArgumentParser(
formatter_class=argparse.ArgumentDefaultsHelpFormatter,
description="""
Generate PDF from clusters found un cycles of torque signal.
1) assign labels (name of the maneuvers) to the numeric IDs of clusters.
2) etc.
""",
)
parser.add_argument(
'-n',
type=float,
default=1,
help='multiplicity of the other time-series',
)
parser.add_argument(
'-nbin',
type=int,
default=35,
help="""number of bins for the resampled/rebined time profiles. Also is the lower 
threshold for the physical time span in [sec].""",
)
pa = parser.parse_args()


start = 0
end   = 2*np.pi
step  = 0.1
k     = 3. #1.5

x1 = np.arange(start, end, (1/pa.n)*k*step)
x2 = np.arange(start, end/k, step)

noise = np.random.uniform(0, 2*np.pi, x1.size) / 10

y1 = np.sin(x1) + 1*np.sin(2*x1) + noise
y2 = np.sin(k*x2) + 1*np.sin(k*2*x2)
#y1[11:15] += 15.
#x = np.array([1, 1, 2, 3, 2, 0])
#y = np.array([0, 1, 1, 2, 3, 2, 1])

#-- figure
fig = figure(1, figsize=(14, 6))
ax1 = fig.add_subplot(1, 2, 1)
ax2 = fig.add_subplot(1, 2, 2)

ax1.plot(x1, y1, '-ob', linewidth=1.0, ms=2, label='N:%d' % y1.size)
#plt.setp(ax1, color='b', linewidth=1.0, ms=2, label='N:%d' % y1.size)

ax1.plot(x2, y2, '-or', linewidth=1.0, ms=2, label='N:%d' % y2.size)
#plt.setp(ax2, color='r', linewidth=1.0, ms=2, label='N:%d' % y2.size)


path, cost, dist = dtw_funcs.dtw(y1, y2, metric=euclidean)

for [mx1, mx2] in path:

    #print(mx1, x1[mx1], ':', mx2, x2[mx2])

    ax1.plot([x1[mx1], x2[mx2]], [y1[mx1], y2[mx2]], 'g', alpha=0.3)

ax1.legend(loc='best')


#--- 2nd panel
ax2.imshow(dist , cmap=plt.cm.gray)
for p in path:
    ax2.scatter(*p, color='r', s=1)

ax2.invert_yaxis()
ax2.set_title("condensed distance matrix")

fig.show()
