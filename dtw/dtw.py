#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Custom quick DTW (Dynamic Time Warping).

Source:
https://nipunbatra.github.io/blog/2014/dtw.html
"""
import numpy as np
from scipy.spatial.distance import euclidean



def calc_accum_cost(n1, n2, dist):

    accum_cost = np.zeros((n1, n2))

    accum_cost[0,0] = dist[0,0]

    for i in range(1, n2):

        accum_cost[0,i] = dist[0,i] + accum_cost[0,i-1]

    for i in range(1, n1):

        accum_cost[i,0] = dist[i,0] + accum_cost[i-1,0]

    for i in range(1, n1):

        for j in range(1, n2):

            accum_cost[i,j] = min(accum_cost[i-1, j-1], accum_cost[i-1, j], accum_cost[i, j-1]) + dist[i,j]

    return accum_cost



def dtw(x1, x2, metric=euclidean):

    n1 = len(x1)
    n2 = len(x2)
    distances = np.zeros((n2, n1))

    for i in range(n2):

        for j in range(n1):

            distances[i,j] = metric(x1[j], x2[i])

    accumulated_cost = calc_accum_cost(n1, n2, distances.T)

    path, cost = path_cost(x1, x2, accumulated_cost.T, distances)

    return path, cost, distances



def path_cost(x1, x2, accum_cost, dist):

    n1, n2 = len(x1), len(x2)

    path = [[n1-1, n2-1]]
    cost = 0

    i = n2 - 1
    j = n1 - 1

    while i>0 and j>0:

        if i==0:

            j = j - 1

        elif j==0:

            i = i - 1

        else:

            _min_cost = min(accum_cost[i-1, j-1], accum_cost[i-1, j], accum_cost[i, j-1])

            if accum_cost[i-1,j] == _min_cost:

                i = i - 1

            elif accum_cost[i, j-1] == _min_cost:

                j = j - 1

            else:

                i = i - 1
                j = j - 1

        path.append([j, i])

    path.append([0,0])

    for [_x2, _x1] in path:

        cost = cost + dist[_x1, _x2]

    return path, cost



#EOF
