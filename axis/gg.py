#!/usr/bin/env ipython
# -*- coding: utf-8 -*-
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import ticker

fig = plt.figure()
ax1 = fig.add_subplot(111)
ax2 = ax1.twiny()
ax3 = ax1.twiny()

# Add some extra space for the second axis at the bottom
fig.subplots_adjust(bottom=0.2)

X = np.linspace(1.,10.,1000)
Y = np.cos(X*2)

ax1.plot(X,Y)
ax1.set_xlabel(r"Original x-axis: $X$")

new_tick_locations = np.array([2., 5., 9.])
# original xlims
vmin, vmax = ax1.get_xlim()

def tick_function(X):
    V = 1/(1+X)
    return ["%.3f" % z for z in V]

#----------------
# Move twinned axis ticks and label from top to bottom
ax2.xaxis.set_ticks_position("bottom")
ax2.xaxis.set_label_position("bottom")

# Offset the twin axis below the host
ax2.spines["bottom"].set_position(("axes", -0.15))

# Turn on the frame for the twin axis, but then hide all 
# but the bottom spine
ax2.set_frame_on(True)
ax2.patch.set_visible(False)
for sp in ax2.spines.itervalues():
    sp.set_visible(False)
ax2.spines["bottom"].set_visible(True)

ax2.set_xticks(new_tick_locations)
ax2.set_xticklabels(tick_function(new_tick_locations))
ax2.set_xlabel(r"Modified x-axis: $1/(1+X)$")
ax2.set_xlim(vmin, vmax)

#------------------
# Move twinned axis ticks and label from top to bottom
ax3.xaxis.set_ticks_position("bottom")
ax3.xaxis.set_label_position("bottom")

# Offset the twin axis below the host
ax3.spines["bottom"].set_position(("axes", -0.30))

# Turn on the frame for the twin axis, but then hide all 
# but the bottom spine
ax3.set_frame_on(True)
ax3.patch.set_visible(False)
for sp in ax3.spines.itervalues():
    sp.set_visible(False)
ax3.spines["bottom"].set_visible(True)

new_tick_labels = np.power(10.,new_tick_locations)
ax3.set_xlabel(r"$\lambda$")
ax3.set_xscale('log')
ax3.set_xticks(new_tick_locations)
#ax3.set_xticklabels(tick_function(new_tick_locations))
ax3.set_xticklabels(new_tick_labels)
ax3.get_xaxis().set_major_formatter(ticker.ScalarFormatter())
#ax3.set_xlim(np.power(10.,vmin), np.power(10.,vmax))
ax3.set_xlim(vmin, vmax)

#------------------
fig.savefig('./test.png', dpi=135, bbox_inches='tight')
plt.close(fig)
#EOF
