#!/usr/bin/env ipython
# -*- coding: utf-8 -*-

print ' yo\t:Robot.'

pd = {'jim': 45.6, 'jim2': 3e44, 'jjj': 'jimmy %e'%(5e-6)}
print '\n {jim:+1.3f} IS A {jim2:+03.3e} BUT {jjj} ADEMAS {jim}' .format(**pd)

jim=45.6
print '\n {jim:+1.3f} IS A {jim2:+03.3e} BUT {jjj} ADEMAS {jim}' .format(jim=jim, jim2=3e44, jjj=pd['jjj'])

a, b = 44, 4.5
print '\n a: %d, b:%3.3e, y ademas a:%d, ademas %s' % (a, b, a, 'JIM-VICKY')
#fname2 = 'odjwie_a.%d_odij/weoidjwo_a.%d_b.%e_txt' % (a, h, a, b)

dir_out = 'ThisDIR'
yyyy = 1999
fname_out = '{dir_out}/test_{year:04d}.h5' .format(dir_out=dir_out, year=yyyy)
print fname_out

