from __future__ import division
from __future__ import print_function
import numpy as np
from pylab import *
from lmfit import minimize, Parameters, Parameter, report_errors


#=================================================================
#             MODEL

def r01_p(eps2, th):
	c=cos(th)
	s=(sin(th))**2

	stev= sqrt(eps2) * c - sqrt(1-(s / eps2))
	imen= sqrt(eps2) * c + sqrt(1-(s / eps2))
	return stev/imen

def r01_s(eps2, th):
	c=cos(th)
	s=(sin(th))**2

	stev= c - sqrt(eps2) * sqrt(1-(s/eps2))
	imen= c + sqrt(eps2) * sqrt(1-(s/eps2))
	return stev/imen


def rho(eps2, th):
	return r01_p(eps2, th)/r01_s(eps2, th)

def psi(eps2, th):
	x1=abs(r01_p(eps2, th))
	x2=abs(r01_s(eps2, th))
	return np.arctan2(x1,x2)

#=================================================================
#                   REAL FIT
#

#%%

# generate data from model  
th=linspace(deg2rad(45),deg2rad(70),70-45)
error=0.01
var_re=np.random.normal(size=len(th), scale=error)
data = psi(2,th) + var_re

# residual function
def residuals(params, th, data):
	eps2 = params['eps2'].value

	diff = psi(eps2, th) - data
	return diff

# create a set of Parameters
params = Parameters()
params.add('eps2',   value= 1.0,  min=1.1, max=3.0)


# do fit, here with leastsq model
METHOD="anneal"
result = minimize(residuals, params, args=(th, data), method=METHOD)

# calculate final result
#final = data + result.residual

# write error report
print(" -----> fit_metodo: %s" % METHOD)
report_errors(params)
