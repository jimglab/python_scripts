#!/usr/bin/env python3
from uvicorn.config import Config
from uvicorn.server import Server
from funcs import app

if __name__ == "__main__":
    config = Config(app=app, host="127.0.0.1", port=8000, log_level="info", reload=True)
    server = Server(config=config)
    server.run()


#EOF
