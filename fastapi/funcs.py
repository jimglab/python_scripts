# app.py
from fastapi import FastAPI, Depends, APIRouter
from typing import Dict, List
from functools import partial
from pydantic import BaseModel, Field

app = FastAPI()

@app.get("/")
def read_root():
    return {"Hello": "World"}


@app.get("/hell")
def read_hello(m):
    return {"Hello": m}

class Dummy:

    def __init__(self):
        self.secret = '666'

    @app.get('/one/{two}')
    def one(two: str, r: int):
        return {'message': r}

    #@classmethod
    @app.get('/two/{arg1}')
    def two(cls, arg1):
        return {'message2': arg1}


#++++++++++++++++++++++++++++++++++++++
#class GreetingHandler:
#    def __init__(self, name: str):
#        self.name = name
#
#    def get_greeting(self):
#        return {"message": f"Hello, {self.name}!"}
#
#def get_greeting_handler(name: str):
#    return GreetingHandler(name)
#
#@app.get("/greet/{name}")
#def greet(name: str, handler: GreetingHandler = Depends(get_greeting_handler)):
#    return handler.get_greeting()

#++++++++++++++++++++++++++++++++++++++
#class GreetingHandler(BaseModel):
#    arg1: str
#    arg2: str
#
#    def get_greeting(self, name: str):
#        return {"message": f"Hello, {name}! Your handler has arguments: {self.arg1} and {self.arg2}"}
#
#
#def get_greeting_handler(name: str, obj: GreetingHandler):
#    obj.arg1 += '-' + name
#    return obj
#
#gh = GreetingHandler(arg1="arg1_value", arg2="arg2_value")
#handler_depends = partial(get_greeting_handler, obj=gh)
#
#@app.get("/greet/{name}")
#def greet(name: str, handler: GreetingHandler = Depends(handler_depends, use_cache=False)):
#    return handler.get_greeting(name)
#++++++++++++++++++++++++++++++++++++++
#class GreetingHandler(BaseModel):
#    arg1: str
#    arg2: str
#    _instance = None
#
#    def __init__(self, **data):
#        if not GreetingHandler._instance:
#            super().__init__(**data)
#            GreetingHandler._instance = self
#        else:
#            GreetingHandler._instance.arg1 = data.get("arg1")
#            GreetingHandler._instance.arg2 = data.get("arg2")
#
#    @classmethod
#    def get_instance(cls, **data):
#        if not cls._instance:
#            cls._instance = cls(**data)
#        return cls._instance
#
#    def get_greeting(self, name: str):
#        return {"message": f"Hello, {name}! Your handler has arguments: {self.arg1} and {self.arg2}"}
#
#gh = GreetingHandler.get_instance(arg1="", arg2="arg2_value")
#
#def get_greeting_handler(name: str, obj: GreetingHandler):
#    obj.arg1 += name
#    return obj
#
#handler_depends = partial(get_greeting_handler, obj=gh)
#
#@app.get("/greet/{name}")
#def greet(name: str, handler: GreetingHandler = Depends(handler_depends)):
#    return handler.get_greeting(name)
#++++++++++++++++++++++++++++++++++++++
class GreetingHandler(BaseModel):
    arg1: str
    arg2: str
    arg3: list = Field(default_factory=list)

    def get_greeting(self, name: str):
        return {"message": f"Hello, {name}! Your handler has arguments: {self.arg1} and {self.arg2}"}

ml = [666, ]
gh = GreetingHandler(arg1="", arg2="arg2_value", arg3=ml)
#app.state.greeting_handler = gh

#def get_greeting_handler(name: str, app_handler: GreetingHandler = Depends(lambda: gh)):
#    app_handler.arg1 += name
#    return app_handler

#@app.get("/greet/{name}")
#def greet(name: str, handler: GreetingHandler = Depends(lambda: gh)):
#    handler.arg1 += name
#    return handler.get_greeting(name)

#@app.get("/greet/{name}")
#def greet(name: str):
#    gh.arg1 += name
#    return gh.get_greeting(name)


def greet2(name: str, gh2: object):
    gh2.arg1 += name
    gh2.arg3.append(name)
    print('> len:', len(gh2.arg3))
    return gh2.get_greeting(name)

gh.arg3.append(888)
greet2a = partial(greet2, gh2=gh)
greet2_dec = app.get('/greet/{name}')(greet2a)

#++++++++++++++++++++++++++++++++++++++
#class GreetingHandlerState:
#    _instance = None
#    arg1 = ""
#
#    @classmethod
#    def get_instance(cls):
#        if cls._instance is None:
#            cls._instance = cls()
#        return cls._instance
#
#def get_greeting_handler(name: str):
#    state = GreetingHandlerState.get_instance()
#    state.arg1 += name
#    return state
#
#@app.get("/greet/{name}")
#def greet(name: str, handler_state: GreetingHandlerState = Depends(get_greeting_handler)):
#    return {"message": f"Hello, {name}! Your handler has arguments: {handler_state.arg1}"}
#++++++++++++++++++++++++++++++++++++++
class Message(BaseModel):
    txt: str

class MyClass:

    def __init__(self):
        self.name = 'my dummy!'
        self.router = APIRouter()
        self.router.add_api_route("/hellow", self.hello, methods=["GET"])
        self.router.add_api_route("/posdata", self.posdata, methods=["POST"])

    def hello(self, m='v'):
        return {"message": f"Hello from {self.name}!, m=%r" % m}

    def posdata(self, msg: Message, qq=0):
        return {'text': f'{self.name} -> {msg} | qq={qq}'}

# Instantiate MyClass and pass the hello method to FastAPI app
my_class = MyClass()
## Assign the decorated method to the FastAPI app
#router = APIRouter()
#router.add_api_route("/hellow", my_class_instance.hello, methods=["GET"])
app.include_router(my_class.router)

#EOF
