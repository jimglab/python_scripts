#!/usr/bin/env python3
from uvicorn.config import Config
from uvicorn.server import Server
from funcs import app
from pydantic import BaseModel

class Dummy(BaseModel):
    a: str
    b: object
    c: str

    @classmethod
    def func(cls):
        #print(cls.a)
        cls.a = 'dummy a'
        print(cls.a)


class DummyMut:
    def __init__(self):
        self.gg = 'mmm'
        self.l = []

#EOF
