#!/usr/bin/env python3
from fastapi import FastAPI, Depends, APIRouter
from typing import Dict, List
from functools import partial
from pydantic import BaseModel, Field
from uvicorn.config import Config
from uvicorn.server import Server
import asyncio
import threading
import time


class Message(BaseModel):
    txt: str


class MyClass:

    def __init__(self):
        self.name = 'my dummy!'
        self.router = APIRouter()
        self.router.add_api_route("/hellow", self.hello, methods=["GET"])
        self.router.add_api_route("/posdata", self.posdata, methods=["POST"])
        self.coll = []

    async def hello(self, m='v'):
        return {"message": f"Hello from {self.name}!, m=%r" % m}

    async def posdata(self, msg: Message, qq=0):
        self.coll.append(msg)
        print('coll:', self.coll)
        return {'text': f'{self.name} -> {msg} | qq={qq}'}

    async def _run_server(self):
        app = FastAPI()
        app.include_router(self.router)
        config = Config(app=app, host="127.0.0.1", port=8000, log_level="info", reload=True)
        server = Server(config=config)
        await server.serve()

def some_function(duration):
    start_time = time.time()
    while (time.time() - start_time) < duration:
        # Your actual function logic goes here
        print("Function is running...")
        time.sleep(1)

async def main2():
    # Instantiate MyClass and pass the hello method to FastAPI app
    my_class = MyClass()
    loop = asyncio.get_event_loop()
    #stop_event = asyncio.Event()

    server_coroutine = my_class._run_server()
    task = loop.create_task(server_coroutine)

    function_task = loop.run_in_executor(None, some_function, 5)

    await function_task
    #stop_event.set()
    print('HEREEE-1')
    await task

#def main2():
#    # Instantiate MyClass and pass the hello method to FastAPI app
#    my_class = MyClass()
#    server_thread = threading.Thread(target=my_class._run_server, daemon=True)
#    server_thread.start()
#
#    # ... do other tasks here
#
#    # Wait for the server thread to finish before the main thread exits
#    try:
#        server_thread.join()
#    except KeyboardInterrupt:
#        print("\nGracefully shutting down...")
#    finally:
#        # Handle cleanup here if needed
#        pass

#def main2():
#    # Instantiate MyClass and pass the hello method to FastAPI app
#    my_class = MyClass()
#    server_thread = threading.Thread(target=my_class._run_server, daemon=True)
#    server_thread.start()

#def main2():
#    # Instantiate MyClass and pass the hello method to FastAPI app
#    my_class = MyClass()
#    server_thread = threading.Thread(target=my_class._run_server, daemon=True)
#    server_thread.start()

#async def main2():
#    # Instantiate MyClass and pass the hello method to FastAPI app
#    my_class = MyClass()
#    new_loop = asyncio.new_event_loop()
#    server_coroutine = my_class._run_server()
#    future = asyncio.run_coroutine_threadsafe(server_coroutine, new_loop)

#def main():
#    app = FastAPI()
#    # Instantiate MyClass and pass the hello method to FastAPI app
#    my_class = MyClass()
#    ## Assign the decorated method to the FastAPI app
#    #router = APIRouter()
#    #router.add_api_route("/hellow", my_class_instance.hello, methods=["GET"])
#    app.include_router(my_class.router)
#
#    config = Config(app=app, host="127.0.0.1", port=8000, log_level="info", reload=True)
#    server = Server(config=config)
#    server.run()
#
#
#async def main2():
#    # Instantiate MyClass and pass the hello method to FastAPI app
#    my_class = MyClass()
#    loop = asyncio.get_event_loop()
#    server_coroutine = my_class._run_server()
#    task = loop.create_task(server_coroutine)
#    #loop.run_until_complete(task)

if __name__ == '__main__':
    #main()
    asyncio.run(main2())
    #main2()

#EOF
