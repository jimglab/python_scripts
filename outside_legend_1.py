import matplotlib
import matplotlib.artist as artists
import matplotlib.pyplot as plt
from matplotlib.pyplot import savefig
from matplotlib.lines import Line2D
 
fig=plt.figure(1, figsize=(3,3))
ax = plt.subplot(111)
 
# Using Line2D to create the markers for the legend. This is the creation of the proxy artists.
club = Line2D(range(1), range(1), color="white", marker=r'$\clubsuit$', markersize=10, markerfacecolor="black")
circle = Line2D(range(1), range(1), color="white", marker='o', markersize=10, markerfacecolor="green")
 
# Calling the handles and labels to create the legend, where the handles are the club and circle created previously, and the labels are what the markers are labeled in the legend. Also moves the legend outside the figure
leg = plt.legend([circle, club],["circle", "club"], loc = "center left", bbox_to_anchor = (1, 0.5), numpoints = 1)
 
fig.subplots_adjust(top=0.8)
fig.savefig('legend_test.png', bbox_extra_artists=(leg,), bbox_inches='tight')
plt.show()
