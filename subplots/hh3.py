import matplotlib.pyplot as plt
from matplotlib.gridspec import GridSpec

def make_ticklabels_invisible(fig):
    for i, ax in enumerate(fig.axes):
        ax.text(0.5, 0.5, "ax%d" % (i+1), va="center", ha="center")
        for tl in ax.get_xticklabels() + ax.get_yticklabels():
            tl.set_visible(False)



# demo 3 : gridspec with subplotpars set.

f = plt.figure()

plt.suptitle("GirdSpec w/ different subplotpars")

gs2 = GridSpec(3, 4)
# left spacing creates space for gs1
#gs2.update(left=0.55, right=0.98, hspace=0.05)
gs2.update(left=0.1, right=0.98, hspace=0.05)
ax1 = plt.subplot(gs2[0,:2])
ax2 = plt.subplot(gs2[1,:2])
ax3 = plt.subplot(gs2[2,:2])

ax4 = plt.subplot(gs2[0,2:])
ax5 = plt.subplot(gs2[1,2:])
ax6 = plt.subplot(gs2[2,2:])

ax6.set_xlabel('x label')
ax2.set_ylabel('y label')
ax2.xaxis.set_ticklabels([])

make_ticklabels_invisible(plt.gcf())

#plt.show()
#export to svg and png
plt.savefig('hh3.png')
#plt.savefig('gridspec.svg')
