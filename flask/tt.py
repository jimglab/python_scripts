#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys, os
import logging
import asyncio
# flask
from flask import Flask
from flask_restful import reqparse, abort, Api, Resource
# to use as Redis client
import redis
import time


def abort_if_todo_doesnt_exist(todo_id):
    if todo_id not in TODOS:
        abort(404, message="Todo {} doesn't exist".format(todo_id))


# TodoList
# shows a list of all todos, and lets you POST to add new tasks
class count_msg(Resource):

    def __init__(self, redis_conn):

        # first instance the parent class, and then out
        # particular initialization
        super(count_msg, self).__init__()

        self.redis_conn = redis_conn

   
    def get(self):
        #klist = redis_con.keys(pattern='/todo*')
        klist = self.redis_conn.keys(pattern='/todo*')
        n_msg = len(klist)
        return {'status':'ok', 'count':n_msg}


class pop_msg(Resource):
    def post(self):
        args  = parser.parse_args()
        task  = args.get('task')
        klist = redis_con.keys(pattern='/todo*')
        todo_id_max = int(max(klist).lstrip('/todo'))
        # pop data
        todo_id = '/todo%d' % todo_id_max
        msg = redis_con.get(todo_id)
        redis_con.delete(todo_id)
        dd = {'status': 'ok', 'message': msg }
        return dd, 200


class push_msg(Resource):
    def post(self):
        args  = parser.parse_args()
        task  = args.get('task')
        klist = redis_con.keys(pattern='/todo*')
        todo_id_max = int(max(klist).lstrip('/todo'))
        # append data
        todo_id = 'todo%i' % (todo_id_max + 1)
        redis_con.set('/'+todo_id, task)
        return {'status':'ok'}, 200



class recv_ntoken(Resource):

    def __init__(self, redis_conn, datad={}):

        # first instance the parent class, and then out
        # particular initialization
        super(recv_ntoken, self).__init__()

        self.redis_conn = redis_conn
        self.datad = datad

    def post(self):
        print('recieved a POST!')
        args = parser.parse_args()
        ntoken_requested = int(args.get('ntokens'))
        ntok_current = int(self.redis_conn.get('/ntokens'))
        ntok_new = ntok_current + ntoken_requested
        self.redis_conn.set('/ntokens', ntok_new)

        self.datad['ntokens'] = ntok_new
    
        return {'status':'ok'}, 200



def flask__token_provoker(app=None, host_flask='127.0.0.1', host_redis='172.19.0.100', 
        port_flask:int=5000, port_redis:int=7777, datad:dict={},
        fname_log='./myflask.log', loglevel=logging.INFO):

    """ Start a Flask server meant to provoke the
    x-guest-token so the MITM proxy container capures it.

    --- Input:
    * host -> str
      host='0.0.0.0' to be visible externally as well (it defaults 
      to 127.0.0.1 and it's **not reachable**).

    * port_flask -> int
      port number where the Flask endpoint is reachable.
    """

    #--- set logger
   # logger = logging.getLogger('NTOKEN-FILELOG')
   # logger.addHandler((logging.FileHandler(fname_log) \
   #     if fname_log!='' else logging.StreamHandler()))
   # logger.setLevel(loglevel)

   # logger.info(' >>> Flask instance...')
    if not app:
        #app = Flask(__name__)
        app = Flask('__main__')

    api = Api(app)

    # Initialize redis client
    redis_conn = redis.StrictRedis(host=host_redis, port=port_redis, charset="utf-8", decode_responses=True)
    # initialize some content in Redis
    redis_conn.set('/todo1', 'build an API')
    redis_conn.set('/todo2', 'profit')

    parser = reqparse.RequestParser()
    parser.add_argument('ntokens')

    ##
    ## Endpoints to call the different methods
    ##
    kws = {
    'redis_conn' : redis_conn,
    'datad' : datad,
    }
    #logger.info(' >>> Adding resource... ')
    api.add_resource(recv_ntoken, '/api/gentoken', resource_class_kwargs=kws)
    #api.add_resource(push_msg, '/api/queue/push')
    #api.add_resource(count_msg, '/api/queue/count')
    #api.add_resource(count_msg, '/api/gentoken')     # (POST) how many token to provoke


    #--- logging for Flask server
    # >> option 1
    logging.basicConfig(filename=fname_log, level=logging.DEBUG)
    # >> option 2
    # https://codehandbook.org/writing-error-log-in-python-flask-web-application/
    from logging.handlers import RotatingFileHandler
    logHandler = RotatingFileHandler(fname_log, maxBytes=1000, backupCount=1)
    flask_loglevel = logging.DEBUG
    logHandler.setLevel(flask_loglevel)
    app.logger.setLevel(flask_loglevel)
    app.logger.addHandler(logHandler)
    

    # host='0.0.0.0' to be visible externally as well (it defaults 
    # to 127.0.0.1 and it's **not reachable**).
    #logger.info(' >>> Starting Flask server... ')
    app.run(host=host_flask, port=port_flask, debug=True,)


def run_flask():
    app = Flask(__name__)
    api = Api(app)

    # Initialize redis client
    redis_host = sys.argv[2]
    redis_port = int(sys.argv[3])
    redis_con = redis.StrictRedis(host=redis_host, port=redis_port, charset="utf-8", decode_responses=True)
    # initialize some content in Redis
    redis_con.set('/todo1', 'build an API')
    redis_con.set('/todo2', 'profit')

    parser = reqparse.RequestParser()
    parser.add_argument('task')

    ##
    ## Endpoints to call the different methods
    ##
    #api.add_resource(pop_msg, '/api/queue/pop')
    #api.add_resource(push_msg, '/api/queue/push')
    api.add_resource(count_msg, '/api/queue/count', resource_class_kwargs={'redis_conn':redis_con})

    # host='0.0.0.0' to be visible externally as well (it defaults 
    # to 127.0.0.1 and it's **not reachable**).
    print('definitely here!')
    app.run(host='0.0.0.0', port=int(sys.argv[1]), debug=True,)
    print('am i here???')


#---------------------
if __name__ == '__main__':


    #--- start token receiver (in another thread) (*)
    loop = asyncio.get_event_loop()
    loop.run_in_executor(None, run_flask)
    print('---->>> After the ASYNCIO non-blocking executor!')
    while 1:
        time.sleep(3)

    #run_flask()

    #--------   N O T E S   ---------
    # (*): to start a function/method in a non-blocking fashion:
    #      https://stackoverflow.com/questions/43437388/is-it-possible-to-start-an-asyncio-event-loop-in-the-background-without-spawning
    #      This actually starts running the function/method but in different thread.

    """
    app = Flask(__name__)
    api = Api(app)

    # Initialize redis client
    redis_host = sys.argv[2]
    redis_port = int(sys.argv[3])
    redis_con = redis.StrictRedis(host=redis_host, port=redis_port, charset="utf-8", decode_responses=True)
    # initialize some content in Redis
    redis_con.set('/todo1', 'build an API')
    redis_con.set('/todo2', 'profit')

    parser = reqparse.RequestParser()
    parser.add_argument('task')

    ##
    ## Endpoints to call the different methods
    ##
    #api.add_resource(pop_msg, '/api/queue/pop')
    #api.add_resource(push_msg, '/api/queue/push')
    api.add_resource(count_msg, '/api/queue/count', resource_class_kwargs={'redis_conn':redis_con})

    # host='0.0.0.0' to be visible externally as well (it defaults 
    # to 127.0.0.1 and it's **not reachable**).
    print('definitely here!')
    app.run(host='0.0.0.0', port=int(sys.argv[1]), debug=True,)
    print('am i here???')
    """

    """
    Requests like below work:

        curl "http://localhost:5002/api/queue/count"

    """
