import matplotlib.pyplot as plt
import mpl_toolkits.mplot3d.axes3d as axes3d

fig = plt.figure(figsize=(4,4))
ax1 = fig.add_subplot(1, 1, 1, projection='3d')

#data

x = [1,2,3,4,5]
y = [23, 34, 23, 34, 45]
z = [3, 4, 6, 7, 5]

ax1.plot(x,y,z)

for i in range(0, 180, 10):
	ax1.azim = i
	#   ax1.elev = 30 
	name = "./figs/anim_%s.png" % i
	plt.savefig(name, format="png")

	#plt.show()
