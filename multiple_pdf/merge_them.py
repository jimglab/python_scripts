#!/usr/bin/env python
# -*- coding: utf-8 -*-
""" source:
http://stackoverflow.com/questions/17104926/pypdf-merging-multiple-pdf-files-into-one-pdf

merge .pdf files
"""

from PyPDF2 import PdfFileMerger, PdfFileReader

filenames = ('one.pdf', 'two.pdf')

merger = PdfFileMerger()
for filename in filenames:
    merger.append(PdfFileReader(file(filename, 'rb')))

merger.write("document-output.pdf")
