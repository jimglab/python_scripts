#!/usr/bin/env python
# -*- coding: utf-8 -*-


from matplotlib import pyplot as plot
from matplotlib.backends.backend_pdf import PdfPages
 
# The PDF document
pdf_pages = PdfPages('my-fancy-document.pdf')
 
for i in xrange(3):
  # Create a figure instance (ie. a new page)
  fig = plot.figure(figsize=(8.27, 11.69), dpi=100)
 
  # Plot whatever you wish to plot
 
  # Done with the page
  pdf_pages.savefig(fig)
 
# Write the PDF document to the disk
pdf_pages.close()
