#!/usr/bin/env python
# -*- coding: utf-8 -*-
""" help:
http://blog.marmakoide.org/?p=94

generate plots in different pages of a .pdf
"""
from matplotlib import pyplot as plot
from matplotlib.backends.backend_pdf import PdfPages
import numpy as np
 
# The PDF document
pdf_pages = PdfPages('my-fancy-document.pdf')

#--- data
x = np.linspace(1., 1e3, 1000)
y = x**3
 
for i in xrange(3):
  # Create a figure instance (ie. a new page)
  fig = plot.figure(figsize=(8.27, 11.69), dpi=100)
  ax  = fig.add_subplot(111)
 
  # Plot whatever you wish to plot
  ax.plot(x, y)
 
  # Done with the page
  pdf_pages.savefig(fig)
 
# Write the PDF document to the disk
pdf_pages.close()
