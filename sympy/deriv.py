#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
 Example extracted from:
 http://docs.sympy.org/dev/tutorial/calculus.html
"""
from sympy import *

x, y, z = symbols('x y z')
init_printing(use_unicode=True)

print diff(cos(x), x)

print diff(exp(x**2), x)

expr = exp(x*y*z)
print diff(expr, x, y, z)
print diff(expr, x, y, 1, z)
print diff(expr, x, y, 2, z)
