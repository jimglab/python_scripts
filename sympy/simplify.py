#!/usr/bin/env ipython
# -*- coding: utf-8 -*-
"""
source:
http://docs.sympy.org/dev/tutorial/simplification.html
"""
from sympy import *
x, y, z = symbols('x y z')
init_printing(use_unicode=True)

print simplify(sin(x)**2 + cos(x)**2)

print simplify((x**3 + x**2 - x - 1)/(x**2 + 2*x + 1))

#x = 1.1  # to evaluate the above expression
print simplify(gamma(x)/gamma(x - 2))

#--- expansions
# Given a polynomial, expand() will put it into a canonical form of a sum of monomials.
print expand((x+1)**2)
