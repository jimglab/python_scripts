#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
 este ejemplo llama un metodo a traves de 
 strings arbitrarios. Si el metodo no esta 
 implmentado salta un AttributeError.
"""

class myclass:
    def __init__(self):
        self.a = 7

    def xyz(self):
        print " ---> original xyz!"

    def one_this(self):
        print " ---onethis... "

    
    def __getattr__(self, aname):
        print " through __getattr__ :P"
        if aname[-2:]=='ii':
            rout = aname+'_ii'
            getattr(self, 'one_this')()
            #return self.rout
        else:
            raise AttributeError
    
            

if __name__=='__main__':
    m = myclass()
    m.one_this_ii
