import numpy as np
from scipy.io import netcdf
f = netcdf.netcdf_file('simple.nc', 'w')

f.history = 'Created for a test'
f.createDimension('time', 10)
time = f.createVariable('time', 'i', ('time',))
time[:] = np.arange(10)
time.units = 'days since 2008-01-01'
f.close()
