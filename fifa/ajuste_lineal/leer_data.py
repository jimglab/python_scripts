from pylab import *
import numpy as np

# hay una opcion "unpack" pero no muy recomendada para
# manejo de datos
# Asi, por ej., la 2da columna es: data[:,1]
data	= np.loadtxt('test.txt')
x	= data[:,0]
y	= data[:,1]

# fiteo lineal
npol	= 1			# grado del polinomio q quiero fitear
p	= np.polyfit(x, y, npol, cov=True)	# 'p' es una lista
m	= p[0][0]		# pendiente
b	= p[0][1]		# ordenada al origen

# ploteemos para ver q en verdad fiteo
#title("mi primer fiteo")
scatter(x, y, c='black', label='data')
plot(x, m*x+b, '-', c='red', lw=3, label='ajuste')
legend(loc='best')	# para mostrar los labels
grid()
xlabel("eje x ($\\theta$)")
ylabel("eje y ($\lambda$)")

# guardamos figura en archivo
savefig('test.png')
close()		# es buena costumbre cerrar la figura

