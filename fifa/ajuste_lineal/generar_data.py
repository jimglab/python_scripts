from pylab import *
import numpy as np

# generamos data
n	= 100
ruido	= np.random.randn(n)
x	= np.linspace(1., 10., n)
y	= 3.*x + ruido

# guardamos data en archivo
data	= np.array([x, y])
np.savetxt('test.txt', data.T, fmt='%3.4g')
