from pylab import *
import numpy as np

# hay una opcion "unpack" pero no muy recomendada para
# manejo de datos
# Asi, por ej., la 2da columna es: data[:,1]
data	= np.loadtxt('test.txt')
x	= data[:,0]
y	= data[:,1]

# fiteo lineal
npol	= 1			# grado del polinomio q quiero fitear
p	= np.polyfit(x, y, npol, cov=True)	# 'p' es una lista
m	= p[0][0]		# pendiente
b	= p[0][1]		# ordenada al origen

# ploteemos para ver q en verdad fiteo
# definimos panel de ploteo
fig	= figure(1, figsize=(6, 4))
ax	= fig.add_subplot(111)

ax.scatter(x, y, c='black', label='data')
ax.plot(x, m*x+b, '-', c='red', lw=3, label='ajuste')
ax.set_legend(loc='best')	# para mostrar los labels
ax.grid()
ax.set_xlabel("eje x ($\\theta$)")
ax.set_ylabel("eje y ($\lambda$)")

# guardamos figura en archivo
fig.savefig('test.png')
close(fig)		# es buena costumbre cerrar la figura

