""" "Async generator"
This runs a task that fills up a list object, and consumes it later.
"""
import asyncio

async def my_generator(results):
    i = 0
    while i < 5:
        await asyncio.sleep(2)
        results.append(i)
        i += 1

async def main():
    results = []
    # Start the generator
    gen_task = asyncio.create_task(my_generator(results))
    print('gen returned')
    #ok = False
    #while (ok is False):
    #    if results and results[-1] != 3:
    #        print(results)
    #    await asyncio.sleep(1)

    # Sleep for 10 seconds
    await asyncio.sleep(10.1)
    print('sleep finished.')

    # Cancel the generator task, in case it's still running
    gen_task.cancel()

    # Print the results
    for i in results:
        print(i)

# Run the main coroutine
asyncio.run(main())



"""
import asyncio

async def my_generator():
    i = 0
    while i < 5:
        await asyncio.sleep(2)
        yield i
        i += 1

# Coroutine that will print the results of the generator
async def main():
    # Create a generator
    gen = my_generator()

    # Sleep for 10 seconds
    await asyncio.sleep(10)

    # Get an asynchronous generator
    agen = gen.__aiter__()

    # Print the values from `agen`
    while True:
        try:
            i = await agen.__anext__()
            print(i)
        except StopAsyncIteration:
            break

# Run the main coroutine
asyncio.run(main())
"""


"""
import asyncio

async def my_generator():
    i = 0
    while i < 5:
        await asyncio.sleep(2)
        yield i
        i += 1

# Coroutine that will print the results of the generator
async def main():
    # Create a generator and start it running as a task
    gen = asyncio.create_task(my_generator().__anext__())

    # Sleep for 10 seconds
    await asyncio.sleep(10)

    # Print the first value from `gen`
    print(await gen)
    
    # Get and print the rest of the values
    while True:
        try:
            gen = asyncio.create_task(gen.__anext__())
            print(await gen)
        except StopAsyncIteration:
            break

# Run the main coroutine
asyncio.run(main())
"""

#EOF
