#!/usr/bin/env python
# -*- coding: utf-8 -*-
import asyncio
import concurrent.futures
import requests

async def main(max_workers=20, nreq=20):
    with concurrent.futures.ThreadPoolExecutor(max_workers=max_workers) as executor:

        loop = asyncio.get_event_loop()
        futures = [
            loop.run_in_executor(
                executor,
                requests.get,
                'http://example.org'
                )
            for i in range(nreq)
            ]

        #for resp in await asyncio.gather(*futures):
        #    print('Done', resp.status_code)
        #    pass
        list_reqs = [ resp for resp in await asyncio.gather(*futures) ]

    return list_reqs


loop = asyncio.get_event_loop()
l = loop.run_until_complete(main())

#EOF
