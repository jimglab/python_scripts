
import asyncio
from contextvars import ContextVar

# Define a context variable
my_var = ContextVar('my_var')

async def print_my_var(name):
    # Get the value of the context variable for the current task
    value = my_var.get(None)
    print(f"{name}: {value}")

async def main():
    # Set the value of the context variable
    my_var.set('Hello')

    # Run two tasks that print the value of the context variable
    await asyncio.gather(print_my_var('Task 1'), print_my_var('Task 2'))

    # Change the value of the context variable
    my_var.set('Goodbye')

    # Run two more tasks
    await asyncio.gather(print_my_var('Task 3'), print_my_var('Task 4'))

asyncio.run(main())
