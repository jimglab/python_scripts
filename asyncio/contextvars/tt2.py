
import asyncio
from contextvars import ContextVar

# Create a ContextVar
counter = ContextVar('counter')
counter.set(0)

async def worker():
    # Get the value of the counter
    count = counter.get()

    # Wait for a bit
    await asyncio.sleep(1)

    # Increment the counter
    for _ in range(3):
        counter.set(count + 1)
        count = counter.get()

    print(f"Counter: {counter.get()}")

async def main():
    # Spawn multiple worker tasks
    tasks = [asyncio.create_task(worker()) for _ in range(5)]

    # Wait for all tasks to complete
    await asyncio.gather(*tasks)

    # Print the final value of the counter
    print(f"Final counter: {counter.get()}")

asyncio.run(main())
