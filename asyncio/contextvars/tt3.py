
import asyncio
from contextvars import ContextVar

# Define the ContextVar
var = ContextVar('var', default='default value')

# Define the function that each task will run
async def worker(name):
    print(f'Before: {name} var = {var.get()}')
    var.set(f'{name} value')
    print(f'After: {name} var = {var.get()}')
    await asyncio.sleep(1)

# Create and run the tasks
async def main():
    task1 = asyncio.create_task(worker('Task 1'))
    task2 = asyncio.create_task(worker('Task 2'))
    task3 = asyncio.create_task(worker('Task 3'))

    await asyncio.gather(task1, task2, task3)

asyncio.run(main())
