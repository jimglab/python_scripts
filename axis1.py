import matplotlib.pyplot as plt
from pylab import *
from numpy import *

x = linspace(1e2, 1e4, 100)
y = 2*x

fig 	= figure(1, figsize=(6, 4))
ax1	= fig.add_subplot(111)

ax1.loglog(x, y, '-o', linewidth=0., markersize=3)
ax1.grid()
#ax1 = plt.gca()
ax2 = ax1.twiny()
ax2.set_xticks([100,80,50])
X2LABELS = array(['0','1','2'])
ax2.set_xticklabels(X2LABELS)
ax1.set_xlabel('redshift')
ax2.set_xlabel('age')

show()
