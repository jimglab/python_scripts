import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
from pylab import *
import matplotlib.cm as cm
import matplotlib.mlab as mlab
import numpy

figure(1,figsize=(5,4))
#normal distribution center at x=0 and y=5
x = randn(100000)
y = randn(100000)+5

set_cmap('gray_r')
plt.xlabel("fdf3efr3r")

surf = hist2d(x, y, bins=40, norm=LogNorm())

plt.xlabel("fer..fr.fvrtfxxxx")
cbar = colorbar()
cbar.set_label("f34rfe")

savefig("prueba.png")
show()
