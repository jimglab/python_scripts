#!/usr/bin/env python
# Is better than du -h --max-depth=1
import numpy as np
import os, sys

try:        # si no hay argumento
    dirname     = sys.argv[1]
except:     # directorio "." por defecto
    dirname = '.'
print " ---> analyzing: " + dirname
fname_inp   = '/tmp/__dummy.sizes__'
"""
if dirname!='':
    fname_inp   = '%s/__dummy.sizes__' % dirname
else:
    fname_inp   = '__dummy.sizes__'
"""


# get the sizes of the directory tree
if os.system('du %s --max-depth=1 > %s' % (dirname, fname_inp)) != 0:
    print '\n [-] something wen\'t wrong!\n Perhaps permission issues with:\n %s'%fname_inp

# if user==root creates this `fname_inp` file, let others
# overwrite it in the future
if os.environ['USER']=='root': os.system('chmod 666 %s' % fname_inp)

f = open(fname_inp, 'r')

s   = []
ss  = []
for l in f:
    #s += [ int(l.split()[0]) ]
    try:
        _ind = l.find(l.split()[1])
        dirname = l[_ind:]
        ss += [( int(l.split()[0]), dirname )]
    except ValueError:
        print " --> ValueError exception!\n entering in debug mode... "
        import pdb; pdb.set_trace()

dtype = [('bsize', int), ('dirname', 'S200')]
s = np.array(ss, dtype=dtype)
s.sort(axis=0)  # ordenamos por tamanio

sz, dirname     = np.zeros(s.size), np.zeros(s.size, dtype='S200')
for i in range(s.size):
    sz[i], dirname[i]  = s[i][0], s[i][1]

for i in range(s.size):
    if sz[i]>1e6:
        print '%2.1f GB' % (sz[i]/1e6),
    elif sz[i]>1e3:
        print '%2.1f MB' % (sz[i]/1e3),
    else: #elif sz[i]>1.0:
        print '%2.1f KB' % (sz[i]),
    
    print ':    ', dirname[i][:-1]
    #import pdb; pdb.set_trace()


"""
for i in range(s.size):
    print s[i][0], ': ', s[i][1]
"""
#s = np.array(s)
#print s, s.size
