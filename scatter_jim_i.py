import numpy as np
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from numpy import *

t,x,y,z = loadtxt("myfile.txt", unpack=True)

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
n = 500
c='r'
m='o'
im = ax.scatter(x, y, z, c=t, marker=m)

ax.set_xlabel('X Label')
ax.set_ylabel('Y Label')
ax.set_zlabel('Z Label')

fig.colorbar(im, ax=ax)
plt.show()

