from lmfit import minimize, Parameters
from numpy import *

def residual(params, x, data, eps_data):
    amp = params['amp'].value
    pshift = params['phase'].value
    freq = params['frequency'].value
    decay = params['decay'].value

    model = amp * sin(x * freq  + pshift) * exp(-x*x*decay)

    return (data-model)/eps_data

params = Parameters()
params.add('amp', value=10)
params.add('decay', value=0.007)
params.add('phase', value=0.2)
params.add('frequency', value=3.0)

n	= 10
x	= linspace(1, 10, n)
data	= x*x
eps_data=.1

out = minimize(residual, params, args=(x, data, eps_data))
