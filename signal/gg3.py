import signal
import time
import os

def handler(signum, frame):
    print 'Here you go'
    raise SystemExit

# tells 'signal' to use 'handler' routine if 
# SIGINT is received
signal.signal(signal.SIGINT, handler)

f = open('myfile.txt', 'w')
while 1:
    print " --> jim "

#EOF
