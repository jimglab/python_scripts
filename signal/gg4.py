#!/usr/bin/env ipython
# -*- coding: utf-8 -*-
"""
para ejecutar rutina q puede ser interrumpida por
signal.SIGINT, en cuyo caso se ejecutara la rutina
'mgr::handler' q usara algunas variables de la rutina 
interrumpida para tomar acciones antes de terminar
con la ejecucion del programa.
"""
import signal
import time
import os

class mgr(object):
    def __init__(self):
        self.name = 'jimmy'

        # tells 'signal' to use 'handler' routine if 
        # SIGINT is received
        signal.signal(signal.SIGINT, self.handler)


    def handler(self, signum, frame):
        self.f.close()
        print " ---> deleting: " + self.fname 
        print os.system('rm '+self.fname)
        print 'Here you go'
        raise SystemExit


    def run(self, fname):
        self.fname = fname
        self.f = open(fname, 'w')
        while 1:
            print " --> jim "


fnm = 'testt.txt'
m = mgr()
m.run(fnm)
#EOF
