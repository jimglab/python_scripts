import numpy as np
from pylab import *

#*******************************************
a1 = np.linspace(0., 9., 10)
b1 = a1.copy()

b1[3] = b1[6] = 13.
we = np.ones(10)
we[5]=5.

p = np.polyfit(a1, b1, 1, cov=True, w=np.sqrt(we))
m1 = p[0][0]
"""
b = p[0][1]
err_m = p[1][0][0]
"""
#*******************************************
a = np.linspace(0., 13., 14)
b = a.copy()

b[3] = b[6] = 13.
a[10] = a[11] = a[12] = a[13] = a[5]
b[10] = b[11] = b[12] = b[13] = b[5]

p = np.polyfit(a, b, 1, cov=True)
m2 = p[0][0]
