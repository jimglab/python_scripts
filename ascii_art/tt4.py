
from PIL import Image
import time

# Define the ASCII characters to use, in order from darkest to lightest
ascii_chars = [' ', '.', ':', '-', '=', '+', '*', '#', '%', '@']

# Load the PNG image
img = Image.open('image.png')

# Resize the image to a width of 80 pixels, and calculate the corresponding height
width = 80
height = int(img.height * (width / img.width))
img = img.resize((width, height))

# Convert the image to grayscale
img = img.convert('L')

# Define the number of frames in each fade cycle
num_frames = 20

# Define the fading range (0-1)
fade_range = 0.5

# Generate the ASCII art for the initial frame
ascii_art = ''
for y in range(height):
    line = ''
    for x in range(width):
        brightness = img.getpixel((x, y))
        char_index = int(brightness / (255/len(ascii_chars)))
        line += ascii_chars[min(char_index, len(ascii_chars)-1)]
    ascii_art += line + '\n'

# Generate the ASCII art for each fading frame
ascii_frames = []
for i in range(num_frames):
    ascii_frame = ''
    fade = 1.0 - (i / (num_frames-1)) * fade_range
    for y in range(height):
        line = ''
        for x in range(width):
            brightness = img.getpixel((x, y))
            char_index = int(brightness / (255/len(ascii_chars)))
            char = ascii_chars[min(char_index, len(ascii_chars)-1)]
            line += '\033[38;2;{0};{1};{2}m{3}\033[0m'.format(255, 255, 255, char) if char != ' ' else ' '
        ascii_frame += line + '\n'
    ascii_frames.append(ascii_frame)

    # Wait a short amount of time before generating the next frame
    time.sleep(0.1)

# Print the ASCII art frames in sequence to create the fading animation
for ascii_frame in ascii_frames:
    print(ascii_frame)
    time.sleep(0.1)
