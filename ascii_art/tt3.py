
import time

# Define the ASCII characters to use, in order from darkest to lightest
ascii_chars = [' ', '.', ':', '-', '=', '+', '*', '#', '%', '@']

# Define the width and height of the logo
width = 80
height = 20

# Define the number of frames in each appearance or fade cycle
num_frames = 10

# Define the number of times to repeat the appearance and fade cycle
N_LOOPS = 3

# Define the logo pattern
#logo_pattern = [
#    '          ',
#    '    * *   ',
#    '  *     * ',
#    '*    *    *',
#    '*  *   *  *',
#    '* * * * * *'
#]
logo_pattern = [
    '    .^.',
    '   /   \\',
    '  /     \\',
    ' / /   \\ \\',
    '/_/___\\_\\_\\'
]

# Pad the logo pattern to match the width and height of the logo
logo_width = len(logo_pattern[0])
logo_height = len(logo_pattern)
logo_x_offset = int((width - logo_width) / 2)
logo_y_offset = int((height - logo_height) / 2)
logo_image = []
for y in range(height):
    line = ''
    for x in range(width):
        if y >= logo_y_offset and y < logo_y_offset + logo_height and x >= logo_x_offset and x < logo_x_offset + logo_width:
            line += logo_pattern[y-logo_y_offset][x-logo_x_offset]
        else:
            line += ' '
    logo_image.append(line)

# Define a function to generate the ASCII image for a given brightness value
def get_ascii_image(brightness):
    ascii_image = ''
    for y in range(height):
        line = ''
        for x in range(width):
            if logo_image[y][x] == ' ':
                line += ' '
            else:
                char_index = int(brightness / (255/len(ascii_chars)))
                line += ascii_chars[min(char_index, len(ascii_chars)-1)]
        ascii_image += line + '\n'
    return ascii_image

# Generate the ASCII images for the appearance and fade cycles
appearance_frames = []
fade_frames = []
for i in range(num_frames):
    brightness = int((i+1) / num_frames * 255)
    appearance_frames.append(get_ascii_image(brightness))
    fade_frames.append(get_ascii_image(255-brightness))

# Generate the list of ASCII images for the full cycle of appearance and fade
full_cycle_frames = appearance_frames + fade_frames

# Generate the list of ASCII images for the full animation
ascii_images = []
for i in range(N_LOOPS):
    ascii_images += full_cycle_frames

# Print the ASCII images in sequence to create the animation
for ascii_image in ascii_images:
    print(ascii_image)
    time.sleep(0.1)
