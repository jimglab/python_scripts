import time

# Define the ASCII characters to use, in order from darkest to lightest
ascii_chars = [' ', '.', ':', '-', '=', '+', '*', '#', '%', '@']

# Define the width and height of the logo
width = 80
height = 20

# Define the number of frames in the animation
num_frames = 20

# Define the number of loops for the animation
n_loops = 5

# Generate the ASCII images for each frame
ascii_images = []
for i in range(num_frames):
    # Calculate the height of the logo for this frame
    logo_height = int(height * (i+1) / num_frames)
    
    # Create a new blank image for this frame
    ascii_image = ''
    
    # Add the top half of the logo
    for y in range(logo_height):
        line = ''
        for x in range(width):
            if x < width/2 - y or x > width/2 + y:
                line += ' '
            else:
                brightness = int((y+1) / logo_height * 255)
                char_index = int(brightness / (255/len(ascii_chars)))
                line += ascii_chars[min(char_index, len(ascii_chars)-1)]
        ascii_image += line + '\n'
    
    # Add the bottom half of the logo
    for y in range(height - logo_height):
        line = ''
        for x in range(width):
            if x < width/2 - (height-y-1) or x > width/2 + (height-y-1):
                line += ' '
            else:
                brightness = int((height-y) / logo_height * 255)
                char_index = int(brightness / (255/len(ascii_chars)))
                line += ascii_chars[min(char_index, len(ascii_chars)-1)]
        ascii_image += line + '\n'
    
    # Add this frame to the list of ASCII images
    ascii_images.append(ascii_image)
    
    # Wait a short time before generating the next frame
    time.sleep(0.1)

# Print the ASCII images in sequence to create the animation
for i in range(n_loops):
    for ascii_image in ascii_images:
        print(ascii_image)
    for ascii_image in reversed(ascii_images):
        print(ascii_image)
