
from __future__ import print_function
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt 
import numpy as np
from numpy import *
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
from matplotlib.colors import LogNorm

import os, sys 
from pylab import *
import os


HOME            = os.path.expanduser('~')

f=open('%s/Dropbox/Juan/grads_ouput.dat' % HOME)

f.seek(256,os.SEEK_SET)

nx	= 1000
ny	= 1000

#plt.pcolor(b)
x = np.linspace(1,10, nx);
y = x;

for i in range(0, 70):
	print(" ----> ", i)
	b=np.fromfile(f, dtype='>f32',count=nx*ny);	# esto es un vector
	b=np.reshape(b, (nx, ny));			# ahora b es una matriz
	cond = b<1.0e30
	b[~cond] = np.nan
	#-------------------
	fig 	= figure(1, figsize=(6, 4))
	ax	= fig.add_subplot(1,1,1)
	CBMIN	= 15.
	CBMAX	= 50.
	surf    = ax.contourf(x, y, b, facecolors=cm.jet(b), linewidth=0, cmap=cm.jet, alpha=0.9, vmin=CBMIN, vmax=CBMAX)

	m = cm.ScalarMappable(cmap=surf.cmap, norm=surf.norm)
	m.set_array(b)

	axcb = plt.colorbar(m)
	LABEL_COLOR_BAR	= 'label de escala colores'
	axcb.set_label(LABEL_COLOR_BAR, fontsize=20)
	if (i+1)%5==0:
		ax.set_title('mi titulo')
		ax.set_xlabel('mi label x')
		ax.set_ylabel('mi label yyy')
	else:
		ax.set_title('DBZ')
		ax.set_xlabel('otro label x')
		ax.set_ylabel('otro yyy')
	m.set_clim(vmin=CBMIN, vmax=CBMAX)
	ax.grid()
	plt.tight_layout()	# ajusta todo lo q tengas en la figura (textos, labels, y ploteo)

	fname_fig = './test_%03d.png' % i
	savefig(fname_fig, dpi=70, format='png', bbox_inches='tight')

	close(fig)	

