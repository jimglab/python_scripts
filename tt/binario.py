import numpy as np 
import matplotlib as plt
import os
from pylab import *

f=open('grads_ouput.dat')

f.seek(256,os.SEEK_SET)

a=np.fromfile(f,dtype='>f32',count=1000*1000);

b=np.reshape(a,(1000,1000));

#plt.pcolor(b)
x = np.linspace(1,10, 1000);
y=x;

fig=figure(1,figsize=(6,4))
ax=fig.add_subplot(111)


ax.contourf(x, y, b, cmap=plt.cm.bone)
#ax.scatter(x, y, c=b)
show()
close()

#f.close()