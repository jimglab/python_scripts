import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit

def func(x, A, tau, base):
    return A * np.exp(- x/tau) + base

x = np.linspace(0,4,50)
y = func(x, -2.5, 1./1.3, 0.)
yn = y + 0.2*np.random.normal(size=len(x))

popt, pcov 	= curve_fit(func, x, yn)	# 'popt' es el vector de argumentos fiteados
perr		= np.sqrt(np.diag(pcov))	# hallo las desv std a partir de la diagonal de la matriza covarianza 'pcpv'
#-------------------------------------------------- ploteo
#
fig 	= plt.figure(1)
ax 	= fig.add_subplot(111)
#
# create grid
#ax.xaxis.set_major_locator(MultipleLocator(1))
#ax.xaxis.set_minor_locator(MultipleLocator(0.2))
#ax.yaxis.set_major_locator(MultipleLocator(1))
#ax.yaxis.set_minor_locator(MultipleLocator(0.2))
#ax.xaxis.grid(True,'minor')
#ax.yaxis.grid(True,'minor')
ax.xaxis.grid(True,'major',linewidth=1)
ax.yaxis.grid(True,'major',linewidth=2)
#
plt.plot(x, yn, 'ko', label="Original Noised Data")
plt.plot(x, func(x, *popt), 'r-', label="Fitted Curve")
plt.legend()
plt.show()
#-------------------------------------------------------------
