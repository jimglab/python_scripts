from numpy import shape
from Scientific.IO.NetCDF import NetCDFFile
import numpy as np
import math
 
# Write a variable with curvilinear coordinates to NETCDF.
def write_curvilinear(file, var, varname, x_coord, y_coord, nx, ny):
    dimnames=('longitude','latitude')
    dimsizes=(nx,ny)
 
    # Create dimensions:
    file.createDimension('longitude', nx)
    file.createDimension('latitude', ny)
 
    x = file.createVariable('LONGXY', 'd', dimnames)
    y = file.createVariable('LATIXY', 'd', dimnames)
 
    # transfer the coordinate variables:
    x[:] = np.reshape(x_coord,dimsizes,'F')
    y[:] = np.reshape(y_coord,dimsizes,'F')
 
    # Create data variable in NetCDF.
    data = file.createVariable(varname, 'd', dimnames)
 
    # transfer the data variables:
    data[:] = np.reshape(var,dimsizes,'F')
 
# number of time states:
time_coord = [x*math.pi/12 for x in range(30)]
ntime=len(time_coord)
 
# number of points:
nx = 40
ny = 30
 
for k in xrange(ntime):
    # Create a coordinates and data variable
    x_coord = [] 
    y_coord = []
    nodal = []
    index = 0
    for j in xrange(ny):
        ty = float(j) / float(ny-1)
        for i in xrange(nx):
            tx = float(i) / float(nx-1)
            angle = tx * math.pi * 2. * 2.  # [0,4pi]
            x_coord = x_coord + [angle]
            y_coord = y_coord + [ty*4. + math.sin(angle - time_coord[k])]
            nodal = nodal + [index]
            index = index + 1
 
    # Open the file:
    file = NetCDFFile('curvilinear%04d.nc' % k, 'w')
    write_curvilinear(file, nodal, "data", x_coord, y_coord, nx, ny)
    file.close()
