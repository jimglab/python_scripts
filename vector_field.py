from pylab import *
from numpy import ma
 
xmin= -2*pi
xmax=  2*pi
trimx = pi/2
ymin= -2.6
ymax=  2.6
trimy = .4
 
X,Y = meshgrid( arange(xmin,xmax, pi/8),arange(ymin,ymax,.2) )
U = 3.*sin(X) * cosh(Y)
V = 3.*cos(X) * sinh(Y)
 
figure()
M = sqrt(pow(U, 2) + pow(V, 2)) # color by abs
#M = arctan(V/U) # color by angle
Q = quiver( X, Y, U, V, M, units='x', width=0.022, scale=1/0.15) # pivot='mid', 
plot(X, Y, 'k,') # dots: 'k.' 'k,' ',' 's'
axis([xmin+trimx, xmax-trimx, ymin+trimy, ymax-trimy])
title("sin(z)")
 
#plt.savefig("sin_z_vector_field_02_Pengo.svg")
#plt.savefig("sin_z_vector_field_02_Pengo.png")
 
show()
close()
