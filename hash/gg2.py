#!/usr/bin/env python
# -*- coding: utf-8 -*-
from Crypto.Cipher import AES
from Crypto import Random
import numpy as np
from numpy import log

#key = b'Sixteen byte key'
ser = (43, 12, 9, 11, 7)
key = ''
for myid in ser:
    key += '%04d' % myid
ns = len(key)
n  = int(log(ns)/log(2.)) + 1
fill = '9'*(2**n-ns)
key += fill

#iv = Random.new().read(AES.block_size)
iv = Random.new().read(2**n)
cipher = AES.new(key, 7, iv)
msg = iv + cipher.encrypt(b'Attack at dawnn.')
print msg.encode('hex')

#EOF
