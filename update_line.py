#!/usr/bin/env ipython
# -*- coding: utf-8 -*-
""" updates line :D
http://stackoverflow.com/questions/4897359/output-to-the-same-line-overwriting-previous-output-python-2-5"""
import sys
from pylab import pause

for i in range(6):
    sys.stdout.write('jimmy: %d\r'%i)
    sys.stdout.flush()
    pause(1)
