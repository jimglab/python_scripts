"""
from:
http://stackoverflow.com/questions/276052/how-to-get-current-cpu-and-ram-usage-in-python
"""
import psutil, os

pid = os.getpid()
py  = psutil.Process(pid)
# memory usage of my script
memoryUse = py.memory_info()[0]/2.**30  # memory use in GB...I think

# report RAM memory usage
# See:
# https://pythonhosted.org/psutil/#memory
v = psutil.virtual_memory()
# percentage usage calculated as (total - available) / total * 100.
print v.percent 
# perc of avail memory
print 100.*v.available/v.total


#EOF
