#!/usr/bin/env ipython
# -*- coding: utf-8 -*-

def fun(a=88.6, **kargs):
    print " a: ", a
    for nm in kargs.keys():
        print ' '+nm+': ', kargs[nm]
    
    jj = kargs['jj']
    print " ---> ", jj


def fun_2(**kas):
    for nm in kas.keys():
        print nm, kas[nm]


ka = {'kk':32, 'jj':11.1234, }
fun(a=66, **ka)

fun_2(**ka)
