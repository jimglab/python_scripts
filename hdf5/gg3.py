import numpy as np
import h5py
from mpi4py import MPI
""" source:
    http://blog.tremily.us/posts/HDF5/ """

comm = MPI.COMM_WORLD
r    = comm.rank

#f = h5py.File('test.h5', 'w', driver='mpio', comm=comm)
f = h5py.File('test.h5', 'w')

data = np.random.random((4,5))
dpath = 'mydir'
f[dpath+'/mu'] = data 
f[dpath+'/mu'].attrs['hola'] = [100, 200]
f[dpath+'/mu'].attrs['units'] = 'this unit'

f.close()
