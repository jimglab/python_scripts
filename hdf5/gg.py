import numpy as np
import h5py
from mpi4py import MPI

comm = MPI.COMM_WORLD
r    = comm.rank

f = h5py.File('test.h5', 'w', driver='mpio', comm=comm)

dpath = '%d' % r
f[dpath+'/mu'] = r

f.close()
