#!/usr/bin/env ipython
# -*- coding: utf-8 -*-
"""
source:
http://stackoverflow.com/questions/24666197/argparse-combining-parent-parser-subparsers-and-default-values
---
runs:
./example.py -- -g2 23.3 -fn 11
"""
import argparse

# this is the top level parser
parser = argparse.ArgumentParser(description='bla bla',)

import funcs
mm = funcs.MyModule() ## module containing parser
mm2 = funcs.MyModule2() ## module containing parser

# subparsers
subparsers = parser.add_subparsers(description=mm.parser.description,)

#--- config 1st parser
subparser2 = subparsers.add_parser('b', help='subparser 2',
   parents=[mm.parser], formatter_class=argparse.ArgumentDefaultsHelpFormatter)
# besides the args that the parent sets, I can add another here too:
subparser2.add_argument('-g2', help='g2 factor', default=33.66, type=float)

#--- config 2nd parser
subparser3 = subparsers.add_parser('XXX', help='subparser 3',
   parents=[mm2.parser], formatter_class=argparse.ArgumentDefaultsHelpFormatter)

#args = subparser2.parse_args()
#print args
pa = parser.parse_args()
print pa

"""
import argparse

# this is the top level parser
parser = argparse.ArgumentParser(description='bla bla')

# this serves as a parent parser
base_parser = argparse.ArgumentParser(add_help=False)
base_parser.add_argument('-n', help='number', type=int)


# subparsers
subparsers = parser.add_subparsers()
subparser1= subparsers.add_parser('a', help='subparser 1', 
                                   parents=[base_parser])
subparser1.add_argument('g1', help='g1 factor', type=float)

subparser1.set_defaults(n=50, g1=55.6)
subparser2 = subparsers.add_parser('b', help='subparser 2',
                                   parents=[base_parser])
subparser1.add_argument('g2', help='g2 factor', type=float)
subparser2.set_defaults(n=20, g2=33.66)

#args = parser.parse_args()
args = subparser2.parse_args()
print args
"""
