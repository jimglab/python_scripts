#!/usr/bin/env ipython
# -*- coding: utf-8 -*-
import argparse
from datetime import datetime, timedelta

class FooAction(argparse.Action):
    def __init__(self, option_strings, dest, nargs=None, **kwargs):
        if nargs is not None:
            raise ValueError("nargs not allowed")
        super(FooAction, self).__init__(option_strings, dest, **kwargs)
    def __call__(self, parser, namespace, values, option_string=None):
        print '%r %r %r' % (namespace, values, option_string)
        dd,mm,yyyy = map(int, values.split('/'))
        value = datetime(yyyy,mm,dd)
        setattr(namespace, self.dest, value)

parser = argparse.ArgumentParser()
parser.add_argument('--foo', action=FooAction)

pa = parser.parse_args()
#print pa
