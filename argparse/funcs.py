#!/usr/bin/env ipython
# -*- coding: utf-8 -*-
import argparse
from datetime import datetime, timedelta

class FooAction(argparse.Action):
    def __init__(self, option_strings, dest, nargs=None, **kwargs):
        if nargs is not None:
            raise ValueError("nargs not allowed")
        super(FooAction, self).__init__(option_strings, dest, **kwargs)
    def __call__(self, parser, namespace, values, option_string=None):
        print '%r %r %r' % (namespace, values, option_string)
        dd,mm,yyyy = map(int, values.split('/'))
        value = datetime(yyyy,mm,dd)
        setattr(namespace, self.dest, value)

class MyModule(object):
    def __init__(self,):
        self.parser = parser = argparse.ArgumentParser(description="""this gral description...""", add_help=False)
        parser.add_argument('--fnum', '-fn', type=float, default=4.5, help='some fnum decription')
        parser.add_argument('--foo', help='this is foo description.', action=FooAction)
        parser.add_argument('--bar', action=FooAction)

class MyModule2(object):
    def __init__(self,):
        self.parser = parser = argparse.ArgumentParser(description="""this gral description...""", add_help=False)
        parser.add_argument('--fnum2', '-fn', type=float, default=4.5, help='some fnum decription')
        parser.add_argument('--foo2', help='this is foo description.', type=float)
        parser.add_argument('--bar2', action=FooAction)


#EOF
