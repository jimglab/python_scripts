from scipy.io.netcdf import netcdf_file
import os
from pylab import *
from numpy import *
from datetime import datetime, time
import numpy as np
import pylab as pl

minute	= 60		# [seg]
hour	= 60*minute	# [seg]
day	= 24*hour	# [seg]

#***************************************************************************
class fechas:
	def __init__(self, yyyy, mm, dd):
		self.yyyy 	= yyyy
		self.mm		= mm
		self.dd		= dd
	def __init__(self):
		self.yyyy = -1
	def __del__(self):
		del self.yyyy
		del self.mm
		del self.dd
		del self
#***************************************************************************
def date_to_utc(fecha):
    utc = datetime(1970, 1, 1, 0, 0, 0, 0)
    time = (fecha - utc).total_seconds()
    return time
#***************************************************************************
class jimmy:
	def __init__(self):
		self.name	= 'jjjiiimmmyyy'

	@profile
	def __init__(self, dir_inp, dir_dst, mintanks, stations, inp):
		self.fname_inp	= '%s/%04d/red_%04d_%02d_%02d.nc' % (dir_inp, inp.yyyy, inp.yyyy, inp.mm, inp.dd)
		try:
			self.finp	= netcdf_file(self.fname_inp, 'r')		# READ ONLYa
			self.file_ok	= True
		except:
			self.file_ok	= False

		if self.file_ok:
			self.nbins_mev	= self.finp.variables['nbins_mev'].getValue()	# nro de bines MeV
			self.dir_dst	= dir_dst
			self.stations	= stations
			self.mintanks	= mintanks
			self.inp	= inp

	@profile
	def build_stuff(self):
		self.a = np.ones(1e7)

	@profile
	def grab_data(self):
		nstations = len(self.stations)
		# inicializo diccionarios
		self.t	= {}
		self.c	= {}
		for id in self.stations:
			#id	= self.tations[i]
			try:
				dataid	= array(self.finp.variables['hist_id.%04d'%id].data)   # [tiempo, cts, cts, ..., cts]
				nid		= size(dataid, 0)
				name		= '%04d' % id
				self.t[name]	= dataid[:,0]		# columna tiempo
				self.c[name]	= dataid[:,1:]		# las demas columnas (cuentas integrales)
				del dataid

			except:
				print " no data for id: %d" % id
				#continue

	def calc_array_avrs(self):
		wndw	= self.inp.wndw	# [seg]
		sst	= self.inp.sst	# [seg]
		yyyy	= self.inp.yyyy
		mm	= self.inp.mm
		dd	= self.inp.dd
		to	= date_to_utc(datetime(yyyy, mm, dd, 0, 0))	# [sec-utc] tiempo 00:00hs de este dia
		dt	= 1.*day-1*sst	# [sec] tiempo total q abarca el procesamiento de datos
		ntime	= int(dt/sst)
		time	= arange(to, to+dt, sst)			# rejilla temporal --> 47 elementos para sst=30min, dt=(1dia-30min)

		nbins_mev	= self.nbins_mev
		basura		= nan*ones(nbins_mev)	# para rellenar cuando no haya suficientes tanques a un tiempo dado
		self.cts_intgr 	= zeros(ntime*nbins_mev).reshape(ntime, nbins_mev)
		self.ntanques	= zeros(ntime)		# nro de tanques q aportan en c/tiempo (0 por defecto)
		set_stations	= self.stations
		set_time	= self.t.keys()
		
		for j in range(ntime):
			nb	= 0			# contador de tanques

			for id in set_stations:
				name	= '%04d' % id
				if name in set_time:	# ver si este tanque aporta data en el archivo 'finp'
					ti		= time[j] - wndw/2.
					tf		= time[j] + wndw/2.
					cc 		= (self.t[name]>ti) & (self.t[name]<tf)		# defino ventana temporal
		
					if len(find(cc))>0:					# hay datos en esta ventana de tiempo?
						#cts_intgr_id 		= mean(self.c[name][cc, :], axis=0)	# (*1)
						#for i in range(nbins_mev):
						#	self.cts_intgr[j,i] = np.mean(self.c[name][cc, i])
						aa			= self.c[name][cc, :]
						cts_intgr_id		= np.empty(nbins_mev)
						cts_intgr_id 		= aa.mean(axis=0)
						#np.mean(aa, out=cts_intgr_id, axis=0, dtype=np.float64)
						del cts_intgr_id
						#del aa
						#cts_intgr_id		= 1.0e2*pl.rand(nbins_mev)
						#self.cts_intgr[j,:]	+= cts_intgr_id
						nb			+= 1
			"""
			if nb>=self.mintanks:
				self.cts_intgr[j,:] 	/= 1.*nb					# normalizo x el nro de tanques q aportaron
				self.ntanques		= nb
			else:
				self.cts_intgr[j,:]  	= basura				# NaNs si no hay sificientes tanques
				self.ntanques		= 0
		"""
		#
		self.time	= time
		del time
		#del cts_intgr_id
		del basura
		#del cc
		##


	@profile
	def __del__(self):
		print " ------> ELIMINANDO."
		#del self.a
		if self.file_ok:
			self.finp.close()
			del self.finp
			del self.c
			del self.t
			#del self.ntanques
			#del self.cts_intgr
			del self.inp
			del self.stations
		del self
	
	@profile
	def run(self):
		#self.build_stuff()
		self.grab_data()
		self.calc_array_avrs()
		pause(1)
		#self.__del__()
		#pause(2)
##################################################################################################
##################################################################################################
##################################################################################################
class ArrayAvrs:
	#@profile
	def __init__(self, dir_inp, dir_dst, mintanks, stations, inp):
		self.fname_inp	= '%s/%04d/red_%04d_%02d_%02d.nc' % (dir_inp, inp.yyyy, inp.yyyy, inp.mm, inp.dd)
		try:
			self.finp	= netcdf_file(self.fname_inp, 'r')		# READ ONLYa
			self.file_ok	= True
		except:
			self.file_ok	= False

		if self.file_ok:
			self.nbins_mev	= self.finp.variables['nbins_mev'].getValue()	# nro de bines MeV
			self.dir_dst	= dir_dst
			self.stations	= stations
			self.mintanks	= mintanks
			self.inp	= inp
	#@profile
	def __del__(self):
		if self.file_ok:
			self.finp.close()
			del self.finp
			del self.c
			del self.t
			del self.ntanques
			del self.cts_intgr
			del self.inp
			del self.stations

	#@profile
	def grab_data(self):
		nstations = len(self.stations)
		# inicializo diccionarios
		self.t	= {}	
		self.c	= {}
		for id in self.stations:
			#id	= self.tations[i]
			try:
				dataid	= array(self.finp.variables['hist_id.%04d'%id].data)   # [tiempo, cts, cts, ..., cts]
				nid		= size(dataid, 0)
				name		= '%04d' % id
				self.t[name]	= dataid[:,0]		# columna tiempo
				self.c[name]	= dataid[:,1:]		# las demas columnas (cuentas integrales)
				del dataid

			except:
				print " no data for id: %d" % id
				#continue
		self.finp.close()

	#@profile
	def calc_array_avrs(self):
		wndw	= self.inp.wndw	# [seg]
		sst	= self.inp.sst	# [seg]
		yyyy	= self.inp.yyyy
		mm	= self.inp.mm
		dd	= self.inp.dd
		to	= date_to_utc(datetime(yyyy, mm, dd, 0, 0))	# [sec-utc] tiempo 00:00hs de este dia
		dt	= 1.*day-1*sst	# [sec] tiempo total q abarca el procesamiento de datos
		ntime	= int(dt/sst)
		time	= arange(to, to+dt, sst)			# rejilla temporal --> 47 elementos para sst=30min, dt=(1dia-30min)

		nbins_mev	= self.nbins_mev
		basura		= nan*ones(nbins_mev)	# para rellenar cuando no haya suficientes tanques a un tiempo dado
		self.cts_intgr 	= zeros(ntime*nbins_mev).reshape(ntime, nbins_mev)
		self.ntanques	= zeros(ntime)		# nro de tanques q aportan en c/tiempo (0 por defecto)
		set_stations	= self.stations
		set_time	= self.t.keys()

		for j in range(ntime):
			nb	= 0			# contador de tanques

			for id in set_stations:
				name	= '%04d' % id
				if name in set_time:	# ver si este tanque aporta data en el archivo 'finp'
					ti		= time[j] - wndw/2.
					tf		= time[j] + wndw/2.
					cc 		= (self.t[name]>ti) & (self.t[name]<tf)		# defino ventana temporal
					if len(find(cc))>0:					# hay datos en esta ventana de tiempo?
						cts_intgr_id 		= mean(self.c[name][cc, :], axis=0)	# (*1)
						self.cts_intgr[j,:]	+= cts_intgr_id
						nb			+= 1
			if nb>=self.mintanks:
				self.cts_intgr[j,:] 	/= 1.*nb					# normalizo x el nro de tanques q aportaron
				self.ntanques		= nb
			else:
				self.cts_intgr[j,:]  	= basura				# NaNs si no hay sificientes tanques
				self.ntanques		= 0
		#
		self.time	= time
		del time
		#del cts_intgr_id
		del basura
		del cc
		##
			
	#*******************************************
	#@profile
	def save2file(self):
		data_out	= array([self.time, self.cts_intgr[:, 0]])
		#print self.time
		#print shape(data_out.T)
		#print shape(self.cts_intgr[:, 1:])
		data_out	= concatenate([data_out.T, self.cts_intgr[:, 1:]], axis=1)
		dir_dst2	= '%s/%04d' % (self.dir_dst, self.inp.yyyy)
		os.system('mkdir -p %s' % dir_dst2)

		self.fname_out	= '%s/%04d_%02d_%02d.txt' % (dir_dst2, self.inp.yyyy, self.inp.mm, self.inp.dd)
		print " ----> guardando: %s" % self.fname_out
		savetxt(self.fname_out, data_out, fmt='%11.4f')
		del data_out


	# (*1) 	promedio todos los datos q tenga este tanque para c/canal de
	#      	energia. El promedio se hace sobre el tiempo; escoge todos los
	#	tiempos q caigan en la ventana (ti, tf)

	#@profile
	def process(self):
		self.grab_data()		# leo la data hacia las variables 'c' y 't'
		pause(2)
		print " ----> calculando promedio sobre el array..."
		self.calc_array_avrs()		# calculo averages hacia 'self.cts_intgr' (tiempo x energia)
		pause(2)
		self.save2file()		# guardo 'self.cts_intgr' en ascii
		self.__del__()

	#@profile
	def __exit__(self, type, value, traceback ):
		self.__del__()
#
