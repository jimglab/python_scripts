from pylab import *
from numpy import *

@profile
def my_func():
    a = [1] * (10 ** 6)
    b = [2] * (2 * 10 ** 7)
    del b
    return a

@profile
def funccc():
    c = 1 * (10 ** 6)
    cc = 2 * (2 * 10 ** 7)
    del c
    return cc
